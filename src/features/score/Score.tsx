import React from 'react'
import { Batsmen, Bowler } from './BatsmenBowler'
import { ScoreButtons } from './ScoreButtons'
import { TeamScore } from './TeamScore'
import { useSelector } from 'react-redux'
import { RootState } from '../../app/store'
import CurrentOver from './CurrentOver'
import { PlayerState } from '../team/teamSlice'

const Score: React.FC = () => {
  const { gameSetup, teams } = useSelector<RootState, RootState>(
    (state) => state
  )
  const bowler = teams.teams[gameSetup.teamIndexBowling].players.find(
    (player: PlayerState) => player.name === gameSetup.currentBowler
  )
  const strikeBatsman = teams.teams[gameSetup.teamIndexBatting].players.find(
    (player: PlayerState) => player.name === gameSetup.strikeBatsman
  )
  const nonStrikeBatsman = teams.teams[gameSetup.teamIndexBatting].players.find(
    (player: PlayerState) => player.name === gameSetup.nonStrikeBatsman
  )
  if (!bowler || !strikeBatsman || !nonStrikeBatsman) return null
  return (
    <>
      <TeamScore />
      <Batsmen
        bowler={bowler!}
        strikeBatsman={strikeBatsman!}
        nonStrikeBatsman={nonStrikeBatsman!}
        teamIndexBatting={gameSetup.teamIndexBatting}
      />
      <Bowler
        bowler={bowler!}
        strikeBatsman={strikeBatsman!}
        nonStrikeBatsman={nonStrikeBatsman!}
        teamIndexBatting={gameSetup.teamIndexBatting}
      />
      <CurrentOver />
      <ScoreButtons
        bowler={bowler!}
        strikeBatsman={strikeBatsman!}
        nonStrikeBatsman={nonStrikeBatsman!}
        teamIndexBatting={gameSetup.teamIndexBatting}
      />
    </>
  )
}

export default Score
