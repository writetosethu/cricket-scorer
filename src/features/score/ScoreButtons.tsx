import { Button, Form, Header, Modal } from 'semantic-ui-react'
import React, { useState } from 'react'
import styled from 'styled-components'
import { useDispatch } from 'react-redux'
import {
  CommonScoreProps,
  ExtrasType,
  nextBallThunkAction,
  scoreSlice,
} from './scoreSlice'
import ChangeBowlerModal from './ChangeBowlerModal'
import ChangeBatterModal from './ChangeBatterModal'
import { useHistory } from 'react-router-dom'

const Container = styled.div`
  padding-bottom: 50px;
`

const BatterHitBallContainer = styled.div`
  margin-bottom: 20px;
`

const StyledButtonContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin-bottom: 20px;
`

interface SelectedButtonState {
  run: number | null
  extra: ExtrasType | null
  batsmanHitTheBall: boolean | null
}

export interface MoreRunsModalProps {
  open: boolean
  closeModal: () => void
  openModal: () => void
  runs: number | null
  setRuns: (runs: number) => void
}
export const MoreRunsModal: React.FC<MoreRunsModalProps> = (props) => {
  return (
    <Modal
      open={props.open}
      trigger={
        <Button
          toggle
          circular
          content={props.runs != null && props.runs > 6 ? props.runs : '+'}
          onClick={props.openModal}
          active={props.runs != null && props.runs > 6}
        />
      }
    >
      <Modal.Content image>
        <Modal.Description>
          <Form>
            <Form.Field>
              <input
                value={props.runs === null ? '' : (props.runs as number)}
                placeholder="Enter runs"
                onChange={(event) => {
                  props.setRuns(parseInt(event.target.value))
                }}
              />
            </Form.Field>
          </Form>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button content="Cancel" onClick={props.closeModal} positive />
        <Button content="OK" onClick={props.closeModal} positive />
      </Modal.Actions>
    </Modal>
  )
}

export const ScoreButtons: React.FC<CommonScoreProps> = ({
  bowler,
  strikeBatsman,
  nonStrikeBatsman,
  teamIndexBatting,
}) => {
  const [moreRunsModalOpen, setMoreRunsModalOpen] = useState<boolean>(false)
  const history = useHistory()

  const [
    selectedButtonState,
    setSelectedButtonState,
  ] = useState<SelectedButtonState>({
    run: null,
    extra: null,
    batsmanHitTheBall: null,
  })

  const invertRunButtonState = (button: number) => {
    setSelectedButtonState({
      ...selectedButtonState,
      run: button,
    })
  }
  const invertExtrasState = (extra: ExtrasType) => {
    setSelectedButtonState({
      ...selectedButtonState,
      extra: selectedButtonState.extra === extra ? null : extra,
    })
  }

  const clearSelectedButtons = () => {
    setSelectedButtonState({ run: null, extra: null, batsmanHitTheBall: null })
  }
  const dispatch = useDispatch()
  return (
    <Container data-testid="button-container">
      <StyledButtonContainer>
        <Button
          circular
          content="0"
          primary
          toggle
          active={selectedButtonState.run === 0}
          onClick={() => invertRunButtonState(0)}
        />
        <Button
          circular
          content="1"
          primary
          toggle
          active={selectedButtonState.run === 1}
          onClick={() => invertRunButtonState(1)}
        />
        <Button
          circular
          content="2"
          primary
          toggle
          active={selectedButtonState.run === 2}
          onClick={() => invertRunButtonState(2)}
        />
        <Button
          circular
          content="3"
          primary
          toggle
          active={selectedButtonState.run === 3}
          onClick={() => invertRunButtonState(3)}
        />
      </StyledButtonContainer>
      <StyledButtonContainer>
        <Button
          toggle
          circular
          content="4"
          primary
          active={selectedButtonState.run === 4}
          onClick={() => invertRunButtonState(4)}
        />
        <Button
          toggle
          circular
          content="5"
          active={selectedButtonState.run === 5}
          onClick={() => invertRunButtonState(5)}
        />
        <Button
          toggle
          circular
          content="6"
          primary
          active={selectedButtonState.run === 6}
          onClick={() => invertRunButtonState(6)}
        />
        <MoreRunsModal
          open={moreRunsModalOpen}
          closeModal={() => setMoreRunsModalOpen(false)}
          openModal={() => setMoreRunsModalOpen(true)}
          runs={selectedButtonState.run}
          setRuns={(runs) => invertRunButtonState(runs)}
        />
      </StyledButtonContainer>
      <StyledButtonContainer>
        <Button
          toggle
          circular
          basic
          content="W"
          active={selectedButtonState.extra === ExtrasType.Wide}
          onClick={() => invertExtrasState(ExtrasType.Wide)}
        />
        <Button
          toggle
          circular
          basic
          content="N"
          active={selectedButtonState.extra === ExtrasType.NoBall}
          onClick={() => invertExtrasState(ExtrasType.NoBall)}
        />
        <Button
          toggle
          circular
          basic
          content="B"
          active={selectedButtonState.extra === ExtrasType.Bye}
          onClick={() => invertExtrasState(ExtrasType.Bye)}
        />
        <Button
          toggle
          circular
          basic
          content="L"
          active={selectedButtonState.extra === ExtrasType.LegBye}
          onClick={() => invertExtrasState(ExtrasType.LegBye)}
        />
      </StyledButtonContainer>
      {selectedButtonState.extra === ExtrasType.NoBall && (
        <BatterHitBallContainer>
          <Header as="h4">Did batter hit the ball?</Header>
          <Button
            toggle
            content="Yes"
            primary
            active={
              selectedButtonState.batsmanHitTheBall == null
                ? false
                : selectedButtonState.batsmanHitTheBall
            }
            onClick={() =>
              setSelectedButtonState({
                ...selectedButtonState,
                batsmanHitTheBall: true,
              })
            }
          />
          <Button
            toggle
            content="No"
            primary
            active={
              selectedButtonState.batsmanHitTheBall == null
                ? false
                : !selectedButtonState.batsmanHitTheBall
            }
            onClick={() =>
              setSelectedButtonState({
                ...selectedButtonState,
                batsmanHitTheBall: false,
              })
            }
          />
        </BatterHitBallContainer>
      )}
      <StyledButtonContainer>
        <Button
          negative
          content="Wicket"
          disabled={selectedButtonState.run === null}
          onClick={() =>
            dispatch(scoreSlice.actions.showChooseNextBatter(null))
          }
        />
      </StyledButtonContainer>
      <StyledButtonContainer>
        <Button
          fluid
          secondary
          content="Next ball"
          disabled={
            selectedButtonState.run === null ||
            (selectedButtonState.extra === ExtrasType.NoBall &&
              selectedButtonState.batsmanHitTheBall === null)
          }
          onClick={() => {
            if (selectedButtonState.run != null) {
              clearSelectedButtons()
              dispatch(
                nextBallThunkAction({
                  bowler: bowler.name,
                  runs: selectedButtonState.run,
                  strikeBatsman: strikeBatsman.name,
                  nonStrikeBatsman: nonStrikeBatsman.name,
                  teamIndexBatting,
                  extra:
                    selectedButtonState.extra === ExtrasType.NoBall
                      ? selectedButtonState.batsmanHitTheBall
                        ? ExtrasType.NoBallStruckByBatsman
                        : ExtrasType.NoBall
                      : selectedButtonState.extra,
                  changeBatterDetails: null,
                })
              )
            }
          }}
        />
      </StyledButtonContainer>
      <StyledButtonContainer>
        <Button
          fluid
          basic
          color="green"
          content="Complete Innings"
          onClick={() => {
            clearSelectedButtons()
            dispatch(scoreSlice.actions.changeInnings(null))
            history.push('/gameSetup')
          }}
        />
      </StyledButtonContainer>
      <ChangeBowlerModal />
      <ChangeBatterModal
        batterChange={(changeBatterDetails) => {
          if (selectedButtonState.run != null) {
            clearSelectedButtons()
            dispatch(
              nextBallThunkAction({
                bowler: bowler.name,
                runs: selectedButtonState.run,
                strikeBatsman: strikeBatsman.name,
                nonStrikeBatsman: nonStrikeBatsman.name,
                teamIndexBatting,
                extra: selectedButtonState.extra,
                changeBatterDetails,
              })
            )
          }
        }}
      />
    </Container>
  )
}
