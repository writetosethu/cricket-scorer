import { PlayerName, PlayerState } from '../team/teamSlice'
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../../app/store'
import { PlayerNamePayload } from '../gameSetup/gameSetupSlice'
import { loadedStateAction } from '../loadGame/loadGameSlice'

export interface CommonScoreProps {
  bowler: PlayerState
  strikeBatsman: PlayerState
  nonStrikeBatsman: PlayerState
  teamIndexBatting: number
}

export enum ExtrasType {
  Wide = 'W',
  NoBall = 'N',
  NoBallStruckByBatsman = 'Nb',
  Bye = 'B',
  LegBye = 'L',
}

export enum DismissalType {
  Caught = 'Caught',
  Bowled = 'Bowled',
  LBW = 'LBW',
  Stumped = 'Stumped',
  RunOut = 'RunOut',
  HitWicket = 'HitWicket',
  FieldObstruction = 'Obstructing the field',
  RetiredOut = 'Retired out',
  HitTheBallTwice = 'Hit The Ball Twice',
  TimedOut = 'TimedOut',
}

export interface Ball {
  runs: number
  over: number
  ball: number
  bowler: PlayerName
  strikeBatsman: PlayerName
  nonStrikeBatsman: PlayerName
  extraType: ExtrasType | null
  dismissalType: DismissalType | null
  batsmanOut: PlayerName | null
  incomingBatsman: PlayerName | null
}

export interface TeamInnings {
  balls: Ball[]
  currentOver: Ball[]
  score: number
  over: number
  wickets: number
}

export interface ModalState {
  showBowlerChange: boolean
  showBatterChange: boolean
}

export interface ScoreState {
  battingTeam: TeamInnings
  bowlingTeam: TeamInnings
  modal: ModalState
}

const teamInningsInitialState = {
  balls: [],
  currentOver: [],
  score: 0,
  over: 0,
  wickets: 0,
}

export interface ChangeBatterDetails {
  batterOutName: PlayerName
  nextBatterInName: PlayerName
  howOut: DismissalType
}

export interface NextBallActionPayload {
  teamIndexBatting: number
  runs: number
  bowler: PlayerName
  strikeBatsman: PlayerName
  nonStrikeBatsman: PlayerName
  extra: ExtrasType | null
  changeBatterDetails: ChangeBatterDetails | null
}

export interface NextOverChangePayload {
  currentBowler: PlayerName
  nextBowler: PlayerName
  teamIndexBatting: number
}

const initialState: ScoreState = {
  battingTeam: teamInningsInitialState,
  bowlingTeam: teamInningsInitialState,
  modal: {
    showBowlerChange: false,
    showBatterChange: false,
  },
}

export const scoreSlice = createSlice({
  name: 'score',
  initialState,
  reducers: {
    nextBallAction(state, action: PayloadAction<NextBallActionPayload>) {
      const payload = action.payload
      let noOfBallsBowled = noOfLegitimateBallsInOver(
        state.battingTeam.currentOver
      )
      if (!isWideOrNoBall(payload.extra)) noOfBallsBowled += 1
      state.battingTeam.currentOver.push({
        runs: payload.runs,
        over: state.battingTeam.over + 1,
        ball: noOfBallsBowled,
        bowler: payload.bowler,
        strikeBatsman: payload.strikeBatsman,
        nonStrikeBatsman: payload.nonStrikeBatsman,
        extraType: action.payload.extra,
        dismissalType:
          payload.changeBatterDetails !== null
            ? payload.changeBatterDetails.howOut
            : null,
        batsmanOut:
          payload.changeBatterDetails !== null
            ? payload.changeBatterDetails.batterOutName
            : null,
        incomingBatsman:
          payload.changeBatterDetails !== null
            ? payload.changeBatterDetails.nextBatterInName
            : null,
      })
      state.battingTeam.score += payload.runs
      if (payload.changeBatterDetails !== null) {
        state.battingTeam.wickets += 1
      }
      state.modal.showBatterChange = false
    },
    showChooseNextBatter(state, _) {
      state.modal.showBatterChange = true
    },
    closeChooseNextBatter(state, _) {
      state.modal.showBatterChange = false
    },
    showChooseNextBowler(state, action: PayloadAction<null>) {
      state.modal.showBowlerChange = true
    },
    nextOverChange(state, action: PayloadAction<NextOverChangePayload>) {
      state.modal.showBowlerChange = false
      state.battingTeam.balls = state.battingTeam.balls.concat(
        state.battingTeam.currentOver
      )
      state.battingTeam.currentOver = []
      state.battingTeam.over += 1
    },
    showWicketFellModal(state) {
      state.modal.showBatterChange = true
    },
    changeInnings(state, _) {
      const battingTeam = state.battingTeam
      state.battingTeam = state.bowlingTeam
      state.bowlingTeam = battingTeam
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      loadedStateAction,
      (state, action: PayloadAction<RootState>) => {
        state.battingTeam = action.payload.score.battingTeam
        state.bowlingTeam = action.payload.score.bowlingTeam
      }
    )
  },
})

export const nextBallThunkAction = createAsyncThunk(
  'score/nextBall',
  (payload: NextBallActionPayload, { dispatch, getState }) => {
    dispatch(scoreSlice.actions.nextBallAction(payload))
    const { score } = getState() as RootState
    if (noOfLegitimateBallsInOver(score.battingTeam.currentOver) === 6) {
      dispatch(scoreSlice.actions.showChooseNextBowler(null))
    }
  }
)

export const bowlerChangeThunkAction = createAsyncThunk(
  'score/bowlerBatsmanChange',
  (payload: PlayerNamePayload, { dispatch, getState }) => {
    const { score, gameSetup } = getState() as RootState
    if (noOfLegitimateBallsInOver(score.battingTeam.currentOver) === 6) {
      dispatch(
        scoreSlice.actions.nextOverChange({
          currentBowler: gameSetup.currentBowler as string,
          nextBowler: payload.name as string,
          teamIndexBatting: gameSetup.teamIndexBatting,
        })
      )
    }
  }
)

export function isWideOrNoBall(extraType: ExtrasType | null): boolean {
  return (
    extraType === ExtrasType.NoBall ||
    extraType === ExtrasType.Wide ||
    extraType === ExtrasType.NoBallStruckByBatsman
  )
}

export function noOfLegitimateBallsInOver(currentOver: Ball[]): number {
  return currentOver.filter((ball) => !isWideOrNoBall(ball.extraType)).length
}

const DismissalTypesAttributedToBowler = [
  DismissalType.Bowled,
  DismissalType.HitWicket,
  DismissalType.Caught,
  DismissalType.Stumped,
  DismissalType.LBW,
]

export function dismissedByBowler(dismissalType: DismissalType): boolean {
  return DismissalTypesAttributedToBowler.includes(dismissalType)
}

export interface Extras {
  total: number
  wides: number
  noballs: number
  legByes: number
  byes: number
}

export function sumUpExtras(teamInnings: TeamInnings): Extras {
  let balls = teamInnings.balls.concat(teamInnings.currentOver)
  return balls.reduce(
    (acc: Extras, ball: Ball) => {
      if (ball.extraType != null) {
        switch (ball.extraType) {
          case ExtrasType.NoBallStruckByBatsman:
            acc.total += 1
            acc.noballs += 1
            break
          case ExtrasType.NoBall:
            acc.total += ball.runs
            acc.noballs += ball.runs
            break
          case ExtrasType.Wide:
            acc.total += ball.runs
            acc.wides += ball.runs
            break
          case ExtrasType.LegBye:
            acc.total += ball.runs
            acc.legByes += ball.runs
            break
          case ExtrasType.Bye:
            acc.total += ball.runs
            acc.byes += ball.runs
            break
        }
      }
      return acc
    },
    {
      total: 0,
      wides: 0,
      noballs: 0,
      legByes: 0,
      byes: 0,
    }
  )
}

export function getNumberOfOversBowled(teamInnings: TeamInnings): number {
  if (teamInnings.currentOver.length > 0) {
    return teamInnings.currentOver[0].over
  } else if (teamInnings.balls.length > 0) {
    return teamInnings.balls[teamInnings.balls.length - 1].over
  }
  return 0
}

export function getBallsInOver(over: number, teamInnings: TeamInnings): Ball[] {
  if (
    teamInnings.currentOver.length > 0 &&
    teamInnings.currentOver[0].over === over
  ) {
    return teamInnings.currentOver
  } else if (
    teamInnings.balls.length > 0 &&
    teamInnings.balls[teamInnings.balls.length - 1].over >= over
  ) {
    return teamInnings.balls.filter((ball) => ball.over === over)
  }
  return []
}
