import React from 'react'
import { ScoreState } from './scoreSlice'
import { renderWithState } from '../../testHelpers/renderHelpers'
import { TeamScore } from './TeamScore'
import { gameSetupState, teamsState } from '../../testHelpers/testData'
import _fill from 'lodash/fill'

describe('TeamScore', function () {
  const teamScoreState: ScoreState = {
    modal: {
      showBowlerChange: false,
      showBatterChange: false,
    },
    bowlingTeam: {
      balls: [],
      score: 100,
      currentOver: [],
      over: 40,
      wickets: 8,
    },
    battingTeam: {
      balls: [],
      score: 4,
      currentOver: _fill(Array(4), {
        runs: 1,
        over: 6,
        ball: 1,
        bowler: '',
        strikeBatsman: '',
        nonStrikeBatsman: '',
        extraType: null,
        dismissalType: null,
        batsmanOut: null,
        incomingBatsman: null,
      }),
      over: 5,
      wickets: 1,
    },
  }
  const teamScoreStateOneInningsOnly: ScoreState = {
    modal: {
      showBowlerChange: false,
      showBatterChange: false,
    },
    bowlingTeam: {
      balls: [],
      score: 0,
      currentOver: [],
      over: 0,
      wickets: 0,
    },
    battingTeam: {
      balls: [],
      score: 4,
      currentOver: _fill(Array(4), {
        runs: 1,
        over: 6,
        ball: 1,
        bowler: '',
        strikeBatsman: '',
        nonStrikeBatsman: '',
        extraType: null,
        dismissalType: null,
        batsmanOut: null,
        incomingBatsman: null,
      }),
      over: 5,
      wickets: 1,
    },
  }
  it('displays the first team and second team score if this is the second innings', () => {
    const { getByTestId, getByText } = renderWithState(<TeamScore />, {
      teams: teamsState,
      score: teamScoreState,
      gameSetup: gameSetupState,
    })

    expect(getByText('Team1')).toBeInTheDocument()
    expect(getByText('Team2')).toBeInTheDocument()

    expect(getByTestId('batting-team-score').textContent).toEqual('4/1 in 5.4')
    expect(getByTestId('bowling-team-score').textContent).toEqual('100/8 in 40')
  })

  it('display bowling team as not played', () => {
    const { getByTestId, getByText } = renderWithState(<TeamScore />, {
      teams: teamsState,
      score: teamScoreStateOneInningsOnly,
      gameSetup: gameSetupState,
    })

    expect(getByText('Team1')).toBeInTheDocument()
    expect(getByText('Team2')).toBeInTheDocument()

    expect(getByTestId('batting-team-score').textContent).toEqual('4/1 in 5.4')
    expect(getByTestId('bowling-team-score').textContent).toEqual(
      'not played yet'
    )
  })
})
