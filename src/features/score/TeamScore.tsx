import { Grid, Header } from 'semantic-ui-react'
import React from 'react'
import styled from 'styled-components'
import { useSelector } from 'react-redux'
import { RootState } from '../../app/store'
import { noOfLegitimateBallsInOver } from './scoreSlice'

const StyledHeader = styled(Header)`
  &&& {
    color: white;
  }
`

export const TeamScore = () => {
  const { teams, gameSetup, score } = useSelector<RootState, RootState>(
    (state) => state
  )
  const noOfBallsInOverBattingTeam = noOfLegitimateBallsInOver(score.battingTeam.currentOver)
  const noOfBallsInOverBowlingTeam = noOfLegitimateBallsInOver(score.bowlingTeam.currentOver)
  return (
    <Grid columns={2}>
      <Grid.Row color="green">
        <Grid.Column textAlign="left">
          <StyledHeader as="h3">
            {teams.teams[gameSetup.teamIndexBatting].name}
          </StyledHeader>
        </Grid.Column>
        <Grid.Column textAlign="right">
          <StyledHeader as="h3" data-testid="batting-team-score">
            {score.battingTeam.score}/{score.battingTeam.wickets} in{' '}
            {score.battingTeam.over}
            {noOfBallsInOverBattingTeam > 0 && `.${noOfBallsInOverBattingTeam}`}
          </StyledHeader>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row color="grey">
        <Grid.Column textAlign="left">
          <StyledHeader as="h4">
            {teams.teams[gameSetup.teamIndexBowling].name}
          </StyledHeader>
        </Grid.Column>
        <Grid.Column textAlign="right">
          <StyledHeader as="h4" data-testid="bowling-team-score">
            {score.bowlingTeam.over > 0 ||
            score.bowlingTeam.currentOver.length > 0 ? (
              <>
                {score.bowlingTeam.score}/{score.bowlingTeam.wickets} in{' '}
                {score.bowlingTeam.over}
                {noOfBallsInOverBowlingTeam > 0 && `.${noOfBallsInOverBowlingTeam}`}
              </>
            ) : (
              'not played yet'
            )}
          </StyledHeader>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}
