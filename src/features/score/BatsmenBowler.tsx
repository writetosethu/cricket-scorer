import { Image, Table } from 'semantic-ui-react'
import StrikeBatsmanIcon from './strike-batsman-icon.png'
import React from 'react'
import styled from 'styled-components'
import { CommonScoreProps } from './scoreSlice'
import { useDispatch } from 'react-redux'
import { gameSetupSlice } from '../gameSetup/gameSetupSlice'

const StyledTable = styled(Table)`
  &&& {
    overflow-x: auto;
    white-space: nowrap;
  }
`

const TableCellTruncated = styled(Table.Cell)`
  &&& {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    max-width: 180px;
    width: 220px;
  }
`
const StyledBatsman = styled.div`
  &&& {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    max-width: 180px;
    width: 220px;
  }
`

export const Batsmen: React.FC<CommonScoreProps> = (props) => {
  const dispatch = useDispatch()
  return (
    <StyledTable singleLine definition unstackable>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Batters</Table.HeaderCell>
          <Table.HeaderCell>R</Table.HeaderCell>
          <Table.HeaderCell>B</Table.HeaderCell>
          <Table.HeaderCell>4s</Table.HeaderCell>
          <Table.HeaderCell>6s</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell>
            <StyledBatsman>
              <Image
                src={StrikeBatsmanIcon}
                avatar
                data-testid="strike-icon"
                onClick={() =>
                  dispatch(gameSetupSlice.actions.batsmenCrossedOver(null))
                }
              />
              <span data-testid="strike-batsman">
                {props.strikeBatsman.name}
              </span>
            </StyledBatsman>
          </Table.Cell>
          <Table.Cell>{props.strikeBatsman.battingStats.runs}</Table.Cell>
          <Table.Cell>{props.strikeBatsman.battingStats.balls}</Table.Cell>
          <Table.Cell>{props.strikeBatsman.battingStats.fours}</Table.Cell>
          <Table.Cell>{props.strikeBatsman.battingStats.sixes}</Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell>
            <StyledBatsman data-testid="nonStrike-batsman">
              {props.nonStrikeBatsman.name}
            </StyledBatsman>
          </Table.Cell>
          <Table.Cell>{props.nonStrikeBatsman.battingStats.runs}</Table.Cell>
          <Table.Cell>{props.nonStrikeBatsman.battingStats.balls}</Table.Cell>
          <Table.Cell>{props.nonStrikeBatsman.battingStats.fours}</Table.Cell>
          <Table.Cell>{props.nonStrikeBatsman.battingStats.sixes}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <TableCellTruncated textAlign="right">Partnership</TableCellTruncated>
          <Table.Cell>
            {props.strikeBatsman.battingStats.runs +
              props.nonStrikeBatsman.battingStats.runs}
          </Table.Cell>
          <Table.Cell>
            {props.strikeBatsman.battingStats.balls +
              props.nonStrikeBatsman.battingStats.balls}
          </Table.Cell>
          <Table.Cell>
            {props.strikeBatsman.battingStats.fours +
              props.nonStrikeBatsman.battingStats.fours}
          </Table.Cell>
          <Table.Cell>
            {props.strikeBatsman.battingStats.sixes +
              props.nonStrikeBatsman.battingStats.sixes}
          </Table.Cell>
        </Table.Row>
      </Table.Body>
    </StyledTable>
  )
}

export const Bowler: React.FC<CommonScoreProps> = ({ bowler }) => {
  return (
    <StyledTable singleLine definition unstackable>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Bowler</Table.HeaderCell>
          <Table.HeaderCell>O</Table.HeaderCell>
          <Table.HeaderCell>R</Table.HeaderCell>
          <Table.HeaderCell>M</Table.HeaderCell>
          <Table.HeaderCell>W</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {bowler != null && (
          <Table.Row>
            <TableCellTruncated>{bowler.name}</TableCellTruncated>
            <Table.Cell data-testid="bowler-overs">
              {bowler.bowlingStats.overs}
              {bowler.bowlingStats.ballInCurrentOver > 0 &&
                `.${bowler.bowlingStats.ballInCurrentOver}`}
            </Table.Cell>
            <Table.Cell data-testid="bowler-runs">
              {bowler.bowlingStats.runs}
            </Table.Cell>
            <Table.Cell data-testid="bowler-maidens">
              {bowler.bowlingStats.maidens}
            </Table.Cell>
            <Table.Cell data-testid="bowler-wickets">
              {bowler.bowlingStats.wickets}
            </Table.Cell>
          </Table.Row>
        )}
      </Table.Body>
    </StyledTable>
  )
}
