import React from 'react'
import { renderWithState } from '../../testHelpers/renderHelpers'
import { gameSetupState, teamsState } from '../../testHelpers/testData'
import Score from './Score'
import userEvent from '@testing-library/user-event'
import { screen, waitFor, within } from '@testing-library/dom'
import { DismissalType, ExtrasType, sumUpExtras } from './scoreSlice'
import { getAllOptionValues } from 'testHelpers/formHelpers'
import produce from 'immer'
import {
  expectBatsmen,
  expectBattingTeamScore,
  expectBowler,
  scoreAnOver,
} from '../../testHelpers/scoringHelpers'

function expectCurrentOver(...values: string[]) {
  expect(
    screen
      .queryAllByTestId('current-over')
      .map((element) => element.textContent)
  ).toEqual(values)
}

describe('Score Tests', () => {
  it('marks 1 run against bowler and batsman and team', async () => {
    const { getByText, store } = renderWithState(<Score />, {
      teams: teamsState,
      gameSetup: gameSetupState,
    })

    userEvent.click(getByText('1'))
    userEvent.click(getByText('Next ball'))

    await waitFor(() => {
      expectBattingTeamScore('1/0 in 0.1')
      expectBatsmen(1, 1, 0, 0, '2.1')
      expectBatsmen(0, 0, 0, 0, '2.2')
      expectBatsmen(1, 1, 0, 0, 'Partnership')
      expectBowler(1, '0.1', 0, 0, '1.1')
      expectCurrentOver('1')
      expect(store.getState().score.battingTeam.currentOver[0]).toEqual({
        runs: 1,
        over: 1,
        ball: 1,
        bowler: '1.1',
        strikeBatsman: '2.1',
        nonStrikeBatsman: '2.2',
        extraType: null,
        dismissalType: null,
        batsmanOut: null,
        incomingBatsman: null,
      })
    })
  })

  it('allows you to enter 7 runs or more by clicking the + button', async () => {
    const { getByText, getByPlaceholderText, store } = renderWithState(
      <Score />,
      {
        teams: teamsState,
        gameSetup: gameSetupState,
      }
    )

    userEvent.click(getByText('+'))

    await waitFor(() => {
      expect(getByPlaceholderText('Enter runs')).toBeInTheDocument()
    })
    await userEvent.type(getByPlaceholderText('Enter runs'), '7')
    await userEvent.click(getByText('OK'))
    await waitFor(() => {
      expect(getByText('7')).toBeInTheDocument()
      expect(getByText('7')).toHaveClass('active')
    })
    userEvent.click(getByText('N'))
    userEvent.click(getByText('Yes'))
    userEvent.click(getByText('Next ball'))

    await waitFor(() => {
      expectBattingTeamScore('7/0 in 0')
      expectBatsmen(6, 1, 0, 1, '2.1')
      expectBatsmen(0, 0, 0, 0, '2.2')
      expectBatsmen(6, 1, 0, 1, 'Partnership')
      expectBowler(7, '0', 0, 0, '1.1')
      expectCurrentOver('Nb7')
      expect(store.getState().score.battingTeam.currentOver[0]).toEqual({
        runs: 7,
        over: 1,
        ball: 0,
        bowler: '1.1',
        strikeBatsman: '2.1',
        nonStrikeBatsman: '2.2',
        extraType: ExtrasType.NoBallStruckByBatsman,
        dismissalType: null,
        batsmanOut: null,
        incomingBatsman: null,
      })
    })
  })

  it('rotates the strike between batsman when odd runs are scored', async () => {
    const { getByText, getByTestId } = renderWithState(<Score />, {
      teams: teamsState,
      gameSetup: gameSetupState,
    })

    userEvent.click(getByText('1'))
    userEvent.click(getByText('Next ball'))

    await waitFor(() => {
      expect(getByTestId('strike-batsman').textContent).toEqual('2.2')
      expect(getByTestId('nonStrike-batsman').textContent).toEqual('2.1')
    })

    userEvent.click(getByText('3'))
    userEvent.click(getByText('Next ball'))

    await waitFor(() => {
      expect(getByTestId('strike-batsman').textContent).toEqual('2.1')
      expect(getByTestId('nonStrike-batsman').textContent).toEqual('2.2')
    })
  })

  it('rotates the strike when over is complete', async () => {
    const { getByText, getByTestId, getByPlaceholderText } = renderWithState(
      <Score />,
      {
        teams: teamsState,
        gameSetup: gameSetupState,
      }
    )

    const buttonContainer = within(getByTestId('button-container'))
    expect(buttonContainer.getByText('Next ball')).toHaveClass('disabled')

    for (let i = 0; i < 6; i++) {
      userEvent.click(buttonContainer.getByText('0'))
      userEvent.click(buttonContainer.getByText('Next ball'))
      await waitFor(() => {
        expect(getByTestId('strike-batsman').textContent).toEqual('2.1')
        expect(getByTestId('nonStrike-batsman').textContent).toEqual('2.2')
      })
    }

    await waitFor(() => {
      expect(getByText('Next Bowler')).toBeInTheDocument()
    })

    userEvent.selectOptions(getByPlaceholderText('Select bowler'), '1.2')
    userEvent.click(getByText('OK'))

    await waitFor(() => {
      expect(getByTestId('strike-batsman').textContent).toEqual('2.2')
      expect(getByTestId('nonStrike-batsman').textContent).toEqual('2.1')
    })
  })

  it('disables next ball when starting and clears the selected buttons after next ball is clicked', async () => {
    const { getByTestId } = renderWithState(<Score />, {
      teams: teamsState,
      gameSetup: gameSetupState,
    })

    const buttonContainer = within(getByTestId('button-container'))
    expect(buttonContainer.getByText('Next ball')).toHaveClass('disabled')

    userEvent.click(buttonContainer.getByText('1'))
    await waitFor(() => {
      expect(buttonContainer.getByText('1')).toHaveClass('active')
    })
    userEvent.click(buttonContainer.getByText('Next ball'))

    await waitFor(() => {
      expect(buttonContainer.getByText('1')).not.toHaveClass('active')
      expect(buttonContainer.getByText('Next ball')).toHaveClass('disabled')
    })
  })

  it('increments 4 and 6 for the batsman', async () => {
    const { getByText, store } = renderWithState(<Score />, {
      teams: teamsState,
      gameSetup: gameSetupState,
    })

    userEvent.click(getByText('4'))
    userEvent.click(getByText('Next ball'))

    await waitFor(() => {
      expectBatsmen(4, 1, 1, 0, '2.1')
      expectBatsmen(4, 1, 1, 0, 'Partnership')
      expectBowler(4, '0.1', 0, 0, '1.1')
      expectCurrentOver('4')
      expectBattingTeamScore('4/0 in 0.1')
    })

    userEvent.click(getByText('6'))
    userEvent.click(getByText('Next ball'))

    await waitFor(() => {
      expectBatsmen(10, 2, 1, 1, '2.1')
      expectBatsmen(10, 2, 1, 1, 'Partnership')
      expectBowler(10, '0.2', 0, 0, '1.1')
      expectCurrentOver('4', '6')
      expectBattingTeamScore('10/0 in 0.2')
    })
  })

  it('shows choose bowler modal when over is over', async () => {
    const {
      getByPlaceholderText,
      queryByText,
      getByTestId,
      getByText,
    } = renderWithState(<Score />, {
      teams: teamsState,
      gameSetup: gameSetupState,
    })

    const buttonContainer = within(getByTestId('button-container'))
    expect(buttonContainer.getByText('Next ball')).toHaveClass('disabled')

    for (let i = 0; i < 6; i++) {
      userEvent.click(buttonContainer.getByText('1'))
      userEvent.click(buttonContainer.getByText('Next ball'))
      await waitFor(() => {
        expect(buttonContainer.getByText('1')).not.toHaveClass('active')
        if (i < 5) {
          expect(queryByText('Next Bowler')).toBeNull()
        }
      })
    }

    await waitFor(() => {
      expect(getByText('Next Bowler')).toBeInTheDocument()
    })

    userEvent.selectOptions(getByPlaceholderText('Select bowler'), '1.2')
    userEvent.click(getByText('OK'))
    await waitFor(() => {
      expectBowler(0, '0', 0, 0, '1.2')
    })
  })

  it('dont show current bowler when choosing next over', async () => {
    const { getByPlaceholderText, getByText } = renderWithState(<Score />, {
      teams: teamsState,
      gameSetup: gameSetupState,
    })
    scoreAnOver([1, 1, 1, 1, 1, 1])
    await waitFor(() => {
      expect(getByText('Next Bowler')).toBeInTheDocument()
    })
    const bowlerNames = getAllOptionValues(
      () => getByPlaceholderText('Select bowler') as HTMLSelectElement
    )

    expect(bowlerNames).not.toContain('1.1')
  })

  describe('extras tests', () => {
    it('should assign batsman runs if a noball is hit', async () => {
      const { getByTestId, store } = renderWithState(<Score />, {
        teams: teamsState,
        gameSetup: gameSetupState,
      })
      const buttonContainer = within(getByTestId('button-container'))
      userEvent.click(buttonContainer.getByText('2'))
      userEvent.click(buttonContainer.getByText('N'))
      expect(buttonContainer.getByText('Next ball')).toHaveClass('disabled')

      // batsman hits the ball - 1 run to batsman and 1 to No ball
      userEvent.click(buttonContainer.getByText('Yes'))
      expect(buttonContainer.getByText('Next ball')).not.toHaveClass('disabled')
      userEvent.click(buttonContainer.getByText('Next ball'))

      await waitFor(() => {
        expectBatsmen(1, 1, 0, 0, '2.1')
        expectBatsmen(0, 0, 0, 0, '2.2')
        expectBatsmen(1, 1, 0, 0, 'Partnership')
        expectBowler(2, '0', 0, 0, '1.1')
        expectCurrentOver('Nb2')
        expectBattingTeamScore('2/0 in 0')
      })
      expect(store.getState().score.battingTeam.currentOver[0]).toEqual({
        runs: 2,
        over: 1,
        ball: 0,
        bowler: '1.1',
        strikeBatsman: '2.1',
        nonStrikeBatsman: '2.2',
        extraType: ExtrasType.NoBallStruckByBatsman,
        dismissalType: null,
        batsmanOut: null,
        incomingBatsman: null,
      })
      expect(sumUpExtras(store.getState().score.battingTeam)).toEqual({
        total: 1,
        wides: 0,
        noballs: 1,
        legByes: 0,
        byes: 0,
      })
    })

    it('should assign extras runs if a batsman does not strike a noball such as a noball + bye', async () => {
      const { getByTestId, store } = renderWithState(<Score />, {
        teams: teamsState,
        gameSetup: gameSetupState,
      })
      const buttonContainer = within(getByTestId('button-container'))
      userEvent.click(buttonContainer.getByText('2'))
      userEvent.click(buttonContainer.getByText('N'))
      // batsman does not hit the ball - all runs to extras
      userEvent.click(buttonContainer.getByText('No'))
      userEvent.click(buttonContainer.getByText('Next ball'))

      await waitFor(() => {
        expectBatsmen(0, 0, 0, 0, '2.1')
        expectBatsmen(0, 0, 0, 0, '2.2')
        expectBatsmen(0, 0, 0, 0, 'Partnership')
        expectBowler(2, '0', 0, 0, '1.1')
        expectCurrentOver('N2')
        expectBattingTeamScore('2/0 in 0')
      })
      expect(store.getState().score.battingTeam.currentOver[0]).toEqual({
        runs: 2,
        over: 1,
        ball: 0,
        bowler: '1.1',
        strikeBatsman: '2.1',
        nonStrikeBatsman: '2.2',
        extraType: ExtrasType.NoBall,
        dismissalType: null,
        batsmanOut: null,
        incomingBatsman: null,
      })
      expect(sumUpExtras(store.getState().score.battingTeam)).toEqual({
        total: 2,
        wides: 0,
        noballs: 2,
        legByes: 0,
        byes: 0,
      })
    })

    it('should mark a wide as a run against the bowler', async () => {
      const { getByTestId, store } = renderWithState(<Score />, {
        teams: teamsState,
        gameSetup: gameSetupState,
      })
      const buttonContainer = within(getByTestId('button-container'))

      userEvent.click(buttonContainer.getByText('W'))
      expect(buttonContainer.getByText('Next ball')).toHaveClass('disabled')
      userEvent.click(buttonContainer.getByText('1'))
      expect(buttonContainer.getByText('Next ball')).not.toHaveClass('disabled')
      userEvent.click(buttonContainer.getByText('Next ball'))

      await waitFor(() => {
        expectBatsmen(0, 0, 0, 0, '2.1')
        expectBatsmen(0, 0, 0, 0, '2.2')
        expectBatsmen(0, 0, 0, 0, 'Partnership')
        expectBowler(1, '0', 0, 0, '1.1')
        expectCurrentOver('W1')
        expectBattingTeamScore('1/0 in 0')
      })
      expect(store.getState().score.battingTeam.currentOver[0]).toEqual({
        runs: 1,
        over: 1,
        ball: 0,
        bowler: '1.1',
        strikeBatsman: '2.1',
        nonStrikeBatsman: '2.2',
        extraType: ExtrasType.Wide,
        dismissalType: null,
        batsmanOut: null,
        incomingBatsman: null,
      })
    })

    it('dont show change of bowler when 6 legitimate balls havent been bowled', async () => {
      const { getByTestId, queryByText } = renderWithState(<Score />, {
        teams: teamsState,
        gameSetup: gameSetupState,
      })
      const buttonContainer = within(getByTestId('button-container'))

      userEvent.click(buttonContainer.getByText('W'))
      userEvent.click(buttonContainer.getByText('1'))
      userEvent.click(buttonContainer.getByText('Next ball'))

      userEvent.click(buttonContainer.getByText('N'))
      userEvent.click(buttonContainer.getByText('1'))
      userEvent.click(buttonContainer.getByText('Next ball'))

      userEvent.click(buttonContainer.getByText('N'))
      userEvent.click(buttonContainer.getByText('1'))
      userEvent.click(buttonContainer.getByText('Next ball'))

      userEvent.click(buttonContainer.getByText('W'))
      userEvent.click(buttonContainer.getByText('1'))
      userEvent.click(buttonContainer.getByText('Next ball'))

      userEvent.click(buttonContainer.getByText('N'))
      userEvent.click(buttonContainer.getByText('1'))
      userEvent.click(buttonContainer.getByText('Next ball'))

      userEvent.click(buttonContainer.getByText('W'))
      userEvent.click(buttonContainer.getByText('1'))
      userEvent.click(buttonContainer.getByText('Next ball'))

      await waitFor(() => {
        expect(queryByText('Next Bowler')).toBeNull()
      })
    })

    it('should not mark Bye and LB against bowler or batsman', async () => {
      const { getByTestId, store } = renderWithState(<Score />, {
        teams: teamsState,
        gameSetup: gameSetupState,
      })
      const buttonContainer = within(getByTestId('button-container'))

      userEvent.click(buttonContainer.getByText('L'))
      userEvent.click(buttonContainer.getByText('1'))
      userEvent.click(buttonContainer.getByText('Next ball'))

      await waitFor(() => {
        expectBatsmen(0, 1, 0, 0, '2.1')
        expectBatsmen(0, 0, 0, 0, '2.2')
        expectBatsmen(0, 1, 0, 0, 'Partnership')
        expectBowler(0, '0.1', 0, 0, '1.1')
        expectCurrentOver('L1')
        expectBattingTeamScore('1/0 in 0.1')
      })
      expect(store.getState().score.battingTeam.currentOver[0]).toEqual({
        runs: 1,
        over: 1,
        ball: 1,
        bowler: '1.1',
        strikeBatsman: '2.1',
        nonStrikeBatsman: '2.2',
        extraType: ExtrasType.LegBye,
        dismissalType: null,
        batsmanOut: null,
        incomingBatsman: null,
      })

      userEvent.click(buttonContainer.getByText('2'))
      userEvent.click(buttonContainer.getByText('B'))
      userEvent.click(buttonContainer.getByText('Next ball'))

      await waitFor(() => {
        expectBatsmen(0, 1, 0, 0, '2.1')
        expectBatsmen(0, 1, 0, 0, '2.2')
        expectBatsmen(0, 2, 0, 0, 'Partnership')
        expectBowler(0, '0.2', 0, 0, '1.1')
        expectCurrentOver('L1', 'B2')
        expectBattingTeamScore('3/0 in 0.2')
      })
      expect(store.getState().score.battingTeam.currentOver[1]).toEqual({
        runs: 2,
        over: 1,
        ball: 2,
        bowler: '1.1',
        strikeBatsman: '2.2',
        nonStrikeBatsman: '2.1',
        extraType: ExtrasType.Bye,
        dismissalType: null,
        batsmanOut: null,
        incomingBatsman: null,
      })
    })
    it('extra button should reset when retouched', async () => {
      const { getByTestId } = renderWithState(<Score />, {
        teams: teamsState,
        gameSetup: gameSetupState,
      })
      const buttonContainer = within(getByTestId('button-container'))

      userEvent.click(buttonContainer.getByText('L'))
      expect(buttonContainer.getByText('L')).toHaveClass('active')

      userEvent.click(buttonContainer.getByText('L'))
      expect(buttonContainer.getByText('L')).not.toHaveClass('active')
    })

    it('does not rotate strike for a wide with 1 run', async () => {
      const { getByTestId } = renderWithState(<Score />, {
        teams: teamsState,
        gameSetup: gameSetupState,
      })

      const buttonContainer = within(getByTestId('button-container'))
      userEvent.click(buttonContainer.getByText('1'))
      userEvent.click(buttonContainer.getByText('W'))
      userEvent.click(buttonContainer.getByText('Next ball'))

      await waitFor(() => {
        expect(getByTestId('strike-batsman').textContent).toEqual('2.1')
        expect(getByTestId('nonStrike-batsman').textContent).toEqual('2.2')
      })

      userEvent.click(buttonContainer.getByText('2'))
      userEvent.click(buttonContainer.getByText('W'))
      userEvent.click(buttonContainer.getByText('Next ball'))

      await waitFor(() => {
        expect(getByTestId('strike-batsman').textContent).toEqual('2.2')
        expect(getByTestId('nonStrike-batsman').textContent).toEqual('2.1')
      })
    })
  })

  describe('Wicket Tests', () => {
    it('shows next batsmen modal with right option values in when a wicket is gone', async () => {
      const modifiedTeamsState = produce(teamsState, (state) => {
        state.teams[1].players[2].battingStats.howOut = DismissalType.Bowled
      })

      const { getByTestId, getByText, getByPlaceholderText } = renderWithState(
        <Score />,
        {
          teams: modifiedTeamsState,
          gameSetup: gameSetupState,
        }
      )
      const buttonContainer = within(getByTestId('button-container'))

      userEvent.click(buttonContainer.getByText('0'))
      userEvent.click(buttonContainer.getByText('Wicket'))

      await waitFor(() => {
        expect(getByText('Batter out')).toBeInTheDocument()
        const batterOut = getAllOptionValues(
          () => getByPlaceholderText('Choose batter out') as HTMLSelectElement
        )
        expect(batterOut).toContain('2.1')
        expect(batterOut).toContain('2.2')

        expect(getByText('Next batter in')).toBeInTheDocument()
        const nextBatsmenOptions = getAllOptionValues(
          () => getByPlaceholderText('Choose next batter') as HTMLSelectElement
        )
        expect(nextBatsmenOptions).not.toContain('2.1')
        expect(nextBatsmenOptions).not.toContain('2.2')
        expect(nextBatsmenOptions).not.toContain('2.3')
      })
    })

    it('marks wicket against bowler and how out against batsman and shows new batsman on screen', async () => {
      const {
        getByTestId,
        getByText,
        getByPlaceholderText,
        queryByText,
        store,
      } = renderWithState(<Score />, {
        teams: teamsState,
        gameSetup: gameSetupState,
      })
      const buttonContainer = within(getByTestId('button-container'))
      userEvent.click(buttonContainer.getByText('0'))
      userEvent.click(buttonContainer.getByText('Wicket'))
      await waitFor(() => {
        expect(getByText('Batter out')).toBeInTheDocument()
      })
      userEvent.selectOptions(getByPlaceholderText('Choose batter out'), '2.1')
      userEvent.selectOptions(
        getByPlaceholderText('How out'),
        DismissalType.Bowled
      )
      userEvent.selectOptions(getByPlaceholderText('Choose next batter'), '2.4')
      userEvent.click(getByText('OK'))
      await waitFor(() => {
        expect(queryByText('Batter out')).toBeNull()
      })

      expectBatsmen(0, 0, 0, 0, '2.4')
      expectBatsmen(0, 0, 0, 0, '2.2')
      expectBatsmen(0, 0, 0, 0, 'Partnership')
      expectBowler(0, '0.1', 0, 1, '1.1')
      expectBattingTeamScore('0/1 in 0.1')
      expectCurrentOver('Wkt0')
      expect(store.getState().score.battingTeam.currentOver[0]).toEqual({
        runs: 0,
        over: 1,
        ball: 1,
        bowler: '1.1',
        strikeBatsman: '2.1',
        nonStrikeBatsman: '2.2',
        extraType: null,
        dismissalType: DismissalType.Bowled,
        batsmanOut: '2.1',
        incomingBatsman: '2.4',
      })
      expect(store.getState().teams.teams[1].players[0].battingStats).toEqual({
        runs: 0,
        balls: 1,
        fours: 0,
        sixes: 0,
        howOut: DismissalType.Bowled,
        wicketBy: '1.1',
        caughtBy: null,
      })
    })

    it('does not mark wicket against bowler for run out', async () => {
      const {
        getByTestId,
        getByText,
        getByPlaceholderText,
        queryByText,
        store,
      } = renderWithState(<Score />, {
        teams: teamsState,
        gameSetup: gameSetupState,
      })
      const buttonContainer = within(getByTestId('button-container'))
      userEvent.click(buttonContainer.getByText('0'))
      userEvent.click(buttonContainer.getByText('Wicket'))
      await waitFor(() => {
        expect(getByText('Batter out')).toBeInTheDocument()
      })
      userEvent.selectOptions(getByPlaceholderText('Choose batter out'), '2.1')
      userEvent.selectOptions(
        getByPlaceholderText('How out'),
        DismissalType.RunOut
      )
      userEvent.selectOptions(getByPlaceholderText('Choose next batter'), '2.4')
      userEvent.click(getByText('OK'))
      await waitFor(() => {
        expect(queryByText('Batter out')).toBeNull()
      })

      expectBowler(0, '0.1', 0, 0, '1.1')

      expect(store.getState().score.battingTeam.currentOver[0]).toEqual({
        runs: 0,
        over: 1,
        ball: 1,
        bowler: '1.1',
        strikeBatsman: '2.1',
        nonStrikeBatsman: '2.2',
        extraType: null,
        dismissalType: DismissalType.RunOut,
        batsmanOut: '2.1',
        incomingBatsman: '2.4',
      })
      expect(store.getState().teams.teams[1].players[0].battingStats).toEqual({
        runs: 0,
        balls: 1,
        fours: 0,
        sixes: 0,
        howOut: DismissalType.RunOut,
        wicketBy: null,
        caughtBy: null,
      })
    })

    it('closes wicket modal when cancel is clicked', async () => {
      const { getByTestId, getByText, queryByText } = renderWithState(
        <Score />,
        {
          teams: teamsState,
          gameSetup: gameSetupState,
        }
      )
      const buttonContainer = within(getByTestId('button-container'))
      userEvent.click(buttonContainer.getByText('0'))
      userEvent.click(buttonContainer.getByText('Wicket'))
      await waitFor(() => {
        expect(getByText('Batter out')).toBeInTheDocument()
      })
      userEvent.click(getByText('Cancel'))
      await waitFor(() => {
        expect(queryByText('Batter out')).toBeNull()
      })
    })
  })
})
