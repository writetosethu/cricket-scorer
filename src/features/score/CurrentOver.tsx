import React from 'react'
import styled from 'styled-components'
import { Header, Label } from 'semantic-ui-react'
import { useSelector } from 'react-redux'
import { RootState } from '../../app/store'
import { Ball } from './scoreSlice'

const StyledDiv = styled.div`
  margin: 20px 0;
`

const CurrentOver = () => {
  const currentOver = useSelector<RootState, Ball[]>(
    (state) => state.score.battingTeam.currentOver
  )
  return (
    <StyledDiv>
      <Header as="h3" content="Current Over" />
      {currentOver.map((ball, index) => (
        <Label
          data-testid="current-over"
          circular
          color={ball.dismissalType === null ? 'green' : 'red'}
          key={index}
        >
          {ball.extraType != null && ball.extraType}
          {ball.dismissalType !== null && 'Wkt'}
          {ball.runs}
        </Label>
      ))}
    </StyledDiv>
  )
}

export default CurrentOver
