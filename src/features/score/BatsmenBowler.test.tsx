import React from 'react'
import { renderWithState } from '../../testHelpers/renderHelpers'
import { gameSetupState, teamsState } from '../../testHelpers/testData'
import { waitFor, within } from '@testing-library/dom'
import produce from 'immer'
import Score from './Score'
import userEvent from '@testing-library/user-event'

describe('BatsmenBowler Tests', () => {
  it('should show current batsman with strike icon', () => {
    const { getByText } = renderWithState(<Score />, {
      teams: teamsState,
      gameSetup: gameSetupState,
    })

    const strikeBatsmen = within(getByText('2.1').closest('tr') as HTMLElement)
    const nonStrikeBatsmen = within(
      getByText('2.2').closest('tr') as HTMLElement
    )

    expect(strikeBatsmen.getByTestId('strike-icon')).toBeInTheDocument()
    expect(nonStrikeBatsmen.queryByTestId('strike-icon')).toBeNull()
  })

  it('should show batting stats', () => {
    const modifiedTeamState = produce(teamsState, (draft) => {
      draft.teams[1].players[0].battingStats.balls = 3
      draft.teams[1].players[0].battingStats.runs = 10
      draft.teams[1].players[0].battingStats.fours = 0
      draft.teams[1].players[0].battingStats.sixes = 2

      draft.teams[1].players[1].battingStats.balls = 10
      draft.teams[1].players[1].battingStats.runs = 20
      draft.teams[1].players[1].battingStats.fours = 2
      draft.teams[1].players[1].battingStats.sixes = 1
    })

    const { getByText } = renderWithState(<Score />, {
      teams: modifiedTeamState,
      gameSetup: gameSetupState,
    })

    const strikeBatsmen = within(getByText('2.1').closest('tr') as HTMLElement)
    const nonStrikeBatsmen = within(
      getByText('2.2').closest('tr') as HTMLElement
    )

    const partnership = within(
      getByText('Partnership').closest('tr') as HTMLElement
    )

    expect(strikeBatsmen.getByText('3')).toBeInTheDocument()
    expect(strikeBatsmen.getByText('10')).toBeInTheDocument()
    expect(strikeBatsmen.getByText('0')).toBeInTheDocument()
    expect(strikeBatsmen.getByText('2')).toBeInTheDocument()

    expect(nonStrikeBatsmen.getByText('10')).toBeInTheDocument()
    expect(nonStrikeBatsmen.getByText('20')).toBeInTheDocument()
    expect(nonStrikeBatsmen.getByText('2')).toBeInTheDocument()
    expect(nonStrikeBatsmen.getByText('1')).toBeInTheDocument()

    expect(partnership.getByText('13')).toBeInTheDocument()
    expect(partnership.getByText('30')).toBeInTheDocument()
    expect(partnership.getByText('2')).toBeInTheDocument()
    expect(partnership.getByText('3')).toBeInTheDocument()
  })

  it('should show bowling stats', () => {
    const modifiedTeamState = produce(teamsState, (draft) => {
      draft.teams[0].players[0].bowlingStats.overs = 10
      draft.teams[0].players[0].bowlingStats.runs = 100
      draft.teams[0].players[0].bowlingStats.maidens = 7
      draft.teams[0].players[0].bowlingStats.wickets = 8
    })

    const { getByText } = renderWithState(<Score />, {
      teams: modifiedTeamState,
      gameSetup: gameSetupState,
    })

    expect(getByText('10')).toBeInTheDocument()
    expect(getByText('100')).toBeInTheDocument()
    expect(getByText('7')).toBeInTheDocument()
    expect(getByText('8')).toBeInTheDocument()
  })

  it('should switch strike and non strik batsmen when icon is clicked', async () => {
    const { getByTestId } = renderWithState(<Score />, {
      teams: teamsState,
      gameSetup: gameSetupState,
    })

    await waitFor(() => {
      expect(getByTestId('strike-batsman').textContent).toEqual('2.1')
      expect(getByTestId('nonStrike-batsman').textContent).toEqual('2.2')
    })

    userEvent.click(getByTestId('strike-icon'))

    await waitFor(() => {
      expect(getByTestId('strike-batsman').textContent).toEqual('2.2')
      expect(getByTestId('nonStrike-batsman').textContent).toEqual('2.1')
    })
  })
})
