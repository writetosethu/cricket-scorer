import React, { useState } from 'react'
import { Button, Dropdown, Header, Modal } from 'semantic-ui-react'
import styled from 'styled-components'
import { DropdownProps } from 'semantic-ui-react/dist/commonjs/modules/Dropdown/Dropdown'
import { useDispatch, useSelector } from 'react-redux'
import { ChangeBatterDetails, DismissalType, scoreSlice } from './scoreSlice'
import { RootState } from '../../app/store'
import { PlayerName, PlayerState } from '../team/teamSlice'
import { dropdownOption } from '../../app/helpers'

const StyledSelect = styled(Dropdown)`
  &&& {
    margin-bottom: 20px;
  }
`

interface ChangeBatterModalProps {
  batterChange: (changeBatterDetails: ChangeBatterDetails) => void
}

const ChangeBatterModal: React.FC<ChangeBatterModalProps> = (props) => {
  const { score, teams, gameSetup } = useSelector<RootState, RootState>(
    (state) => state
  )
  const [batterOutName, setBatterOutName] = useState<PlayerName>('')
  const [nextBatterInName, setNextBatterInName] = useState<PlayerName>('')
  const [howOut, setHowOut] = useState<DismissalType>(DismissalType.Bowled)
  const dispatch = useDispatch()
  const battersIn = !score.modal.showBatterChange
    ? []
    : teams.teams[gameSetup.teamIndexBatting].players.filter(
        (player: PlayerState) => {
          return (
            player.name !== gameSetup.strikeBatsman &&
            player.name !== gameSetup.nonStrikeBatsman &&
            player.battingStats.howOut === null
          )
        }
      )
  return (
    <Modal open={score.modal.showBatterChange}>
      <Modal.Content>
        <Modal.Description>
          <Header as="h2">Batter out</Header>
          <StyledSelect
            fluid
            placeholder="Choose batter out"
            value={batterOutName}
            options={[
              dropdownOption(gameSetup.strikeBatsman as string),
              dropdownOption(gameSetup.nonStrikeBatsman as string),
            ]}
            onChange={(
              event: React.SyntheticEvent<HTMLElement>,
              data: DropdownProps
            ) => {
              setBatterOutName(data.value as string)
            }}
          />
          <StyledSelect
            fluid
            placeholder="How out"
            value={howOut}
            options={Object.keys(DismissalType).map(dropdownOption)}
            onChange={(
              event: React.SyntheticEvent<HTMLElement>,
              data: DropdownProps
            ) => {
              setHowOut(data.value as DismissalType)
            }}
          />
          <Header as="h2">Next batter in</Header>
          <StyledSelect
            fluid
            placeholder="Choose next batter"
            value={nextBatterInName}
            options={battersIn.map((batter: PlayerState) =>
              dropdownOption(batter.name)
            )}
            onChange={(
              event: React.SyntheticEvent<HTMLElement>,
              data: DropdownProps
            ) => {
              setNextBatterInName(data.value as string)
            }}
          />
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button
          content="Cancel"
          onClick={() =>
            dispatch(scoreSlice.actions.closeChooseNextBatter(null))
          }
          positive
        />
        <Button
          content="OK"
          onClick={() =>
            props.batterChange({ batterOutName, nextBatterInName, howOut })
          }
          positive
        />
      </Modal.Actions>
    </Modal>
  )
}

export default ChangeBatterModal
