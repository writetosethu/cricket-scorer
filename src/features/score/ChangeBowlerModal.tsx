import React, { useState } from 'react'
import { Button, Dropdown, Header, Modal } from 'semantic-ui-react'
import styled from 'styled-components'
import { DropdownProps } from 'semantic-ui-react/dist/commonjs/modules/Dropdown/Dropdown'
import { useDispatch, useSelector } from 'react-redux'
import { bowlerChangeThunkAction } from './scoreSlice'
import { RootState } from '../../app/store'
import { PlayerName, PlayerState } from '../team/teamSlice'
import { dropdownOption } from '../../app/helpers'

const StyledSelect = styled(Dropdown)`
  &&& {
    margin-bottom: 20px;
  }
`

const ChangeBowlerModal = () => {
  const { score, teams, gameSetup } = useSelector<RootState, RootState>(
    (state) => state
  )
  const bowlers = teams.teams[gameSetup.teamIndexBowling].players.filter(
    (player: PlayerState) => player.name !== gameSetup.currentBowler
  )
  const [bowlerName, setBowlerName] = useState<PlayerName>('')
  const dispatch = useDispatch()
  return (
    <Modal open={score.modal.showBowlerChange}>
      <Modal.Content>
        <Modal.Description>
          <Header as="h2">Next Bowler</Header>
          <StyledSelect
            fluid
            placeholder="Select bowler"
            value={bowlerName}
            options={bowlers.map((bowler: PlayerState) =>
              dropdownOption(bowler.name)
            )}
            onChange={(
              event: React.SyntheticEvent<HTMLElement>,
              data: DropdownProps
            ) => {
              setBowlerName(data.value as string)
            }}
          />
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button
          content="OK"
          onClick={() => {
            dispatch(
              bowlerChangeThunkAction({
                name: bowlerName,
              })
            )
          }}
          positive
        />
      </Modal.Actions>
    </Modal>
  )
}

export default ChangeBowlerModal
