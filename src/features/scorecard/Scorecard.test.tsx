import React from 'react'
import { renderWithState } from '../../testHelpers/renderHelpers'
import { gameSetupState, teamsState } from '../../testHelpers/testData'
import { Routes } from '../../Routes'
import {
  expectBatsmen,
  expectBowler,
  scoreAnOver,
} from '../../testHelpers/scoringHelpers'
import { screen, waitFor, within } from '@testing-library/dom'
import userEvent from '@testing-library/user-event'

async function showScorecard() {
  userEvent.click(screen.getByTestId('open-sidebar-button'))
  userEvent.click(screen.getByText(/Scorecard/))
}

describe('Scorecard tests', function () {
  it('shows all batters', async () => {
    renderWithState(
      <Routes />,
      {
        teams: teamsState,
        gameSetup: gameSetupState,
      },
      '/score/edit'
    )

    scoreAnOver([1, 1, 1, 1, 1, 1])
    await showScorecard()
    const byText = within(screen.getByTestId('battingTeam')).getByText
    expectBatsmen(3, 3, 0, 0, '2.1', byText)
    expectBatsmen(3, 3, 0, 0, '2.2', byText)
    expectBowler(6, '0.6', 0, 0, '1.1', byText)
  })

  it('shows batters who have not batted as empty', async () => {
    renderWithState(
      <Routes />,
      {
        teams: teamsState,
        gameSetup: gameSetupState,
      },
      '/score/edit'
    )

    scoreAnOver([1, 1, 1, 1, 1, 1])
    await showScorecard()
    const byText = within(screen.getByTestId('battingTeam')).getByText
    expectBatsmen(3, 3, 0, 0, '2.1', byText)
    expectBatsmen(3, 3, 0, 0, '2.2', byText)
    expectBowler(6, '0.6', 0, 0, '1.1', byText)
    const otherBatsmen = byText('2.3').closest('tr') as HTMLTableRowElement
    expect(otherBatsmen.cells[1].textContent as string).toEqual('')
    expect(otherBatsmen.cells[2].textContent as string).toEqual('')
    expect(otherBatsmen.cells[3].textContent as string).toEqual('')
    expect(otherBatsmen.cells[4].textContent as string).toEqual('')
  })

  it('does not show bowlers who havent bowled', async () => {
    renderWithState(
      <Routes />,
      {
        teams: teamsState,
        gameSetup: gameSetupState,
      },
      '/score/edit'
    )

    scoreAnOver([1, 1, 1, 1, 1, 1])
    await showScorecard()
    const byText = within(screen.getByTestId('battingTeam')).queryByText
    expect(byText(1.2)).toBeNull()
  })

  it('displays extras', async () => {
    renderWithState(
      <Routes />,
      {
        teams: teamsState,
        gameSetup: gameSetupState,
      },
      '/score/edit'
    )
    const buttonContainer = within(screen.getByTestId('button-container'))

    userEvent.click(buttonContainer.getByText('N'))
    userEvent.click(buttonContainer.getByText('2'))
    userEvent.click(buttonContainer.getByText('Yes'))
    userEvent.click(buttonContainer.getByText('Next ball'))

    userEvent.click(buttonContainer.getByText('W'))
    userEvent.click(buttonContainer.getByText('2'))
    userEvent.click(buttonContainer.getByText('Next ball'))

    userEvent.click(buttonContainer.getByText('L'))
    userEvent.click(buttonContainer.getByText('1'))
    userEvent.click(buttonContainer.getByText('Next ball'))

    userEvent.click(buttonContainer.getByText('B'))
    userEvent.click(buttonContainer.getByText('1'))
    userEvent.click(buttonContainer.getByText('Next ball'))

    await showScorecard()

    const extrasRow = screen
      .getByText('Total 5')
      .closest('tr') as HTMLTableRowElement

    expect(screen.getByText('Total 5')).toBeInTheDocument()
    expect(parseInt(extrasRow.cells[1].textContent as string)).toEqual(1) // noball
    expect(parseInt(extrasRow.cells[2].textContent as string)).toEqual(2) // wide
    expect(parseInt(extrasRow.cells[3].textContent as string)).toEqual(1) // Leg bye
    expect(parseInt(extrasRow.cells[4].textContent as string)).toEqual(1) // bye
  })
})
