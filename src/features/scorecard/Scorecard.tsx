import React from 'react'
import styled from 'styled-components'
import { Grid, Header, Image, Tab, Table } from 'semantic-ui-react'
import { useSelector } from 'react-redux'
import { RootState } from '../../app/store'
import { PlayerState, TeamsState } from '../team/teamSlice'
import {
  noOfLegitimateBallsInOver,
  sumUpExtras,
  TeamInnings,
} from '../score/scoreSlice'
import { GameSetupState } from '../gameSetup/gameSetupSlice'
import StrikeBatsmanIcon from '../score/strike-batsman-icon.png'

const StyledTeamContainer = styled.div`
  margin-bottom: 40px;
`

const StyledTable = styled(Table)`
  &&& {
    overflow-x: auto;
    white-space: nowrap;
    margin: 20px 0;
  }
`
const TableCellTruncated = styled(Table.Cell)`
  &&& {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    max-width: 180px;
    width: 220px;
  }
`
const StyledBatsman = styled.div`
  &&& {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    max-width: 180px;
    width: 220px;
  }
`

interface CardProps {
  gameSetup: GameSetupState
  players: PlayerState[]
}

const BowlingCard: React.FC<CardProps> = (props) => {
  return (
    <StyledTable singleLine definition unstackable color="yellow">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Bowler</Table.HeaderCell>
          <Table.HeaderCell>O</Table.HeaderCell>
          <Table.HeaderCell>R</Table.HeaderCell>
          <Table.HeaderCell>M</Table.HeaderCell>
          <Table.HeaderCell>W</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {props.players
          .filter(
            (player) =>
              player.bowlingStats.ballInCurrentOver > 0 ||
              player.bowlingStats.overs > 0
          )
          .map((bowler: PlayerState, index) => (
            <Table.Row key={index}>
              <TableCellTruncated>{bowler.name}</TableCellTruncated>
              <Table.Cell data-testid="bowler-overs">
                {bowler.bowlingStats.overs}
                {bowler.bowlingStats.ballInCurrentOver > 0 &&
                  `.${bowler.bowlingStats.ballInCurrentOver}`}
              </Table.Cell>
              <Table.Cell data-testid="bowler-runs">
                {bowler.bowlingStats.runs}
              </Table.Cell>
              <Table.Cell data-testid="bowler-maidens">
                {bowler.bowlingStats.maidens}
              </Table.Cell>
              <Table.Cell data-testid="bowler-wickets">
                {bowler.bowlingStats.wickets}
              </Table.Cell>
            </Table.Row>
          ))}
      </Table.Body>
    </StyledTable>
  )
}

const BattingCard: React.FC<CardProps> = (props) => {
  return (
    <StyledTable singleLine definition unstackable color="red">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Batters</Table.HeaderCell>
          <Table.HeaderCell>R</Table.HeaderCell>
          <Table.HeaderCell>B</Table.HeaderCell>
          <Table.HeaderCell>4s</Table.HeaderCell>
          <Table.HeaderCell>6s</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {props.players.map((player: PlayerState, index: number) => (
          <Table.Row key={index}>
            <Table.Cell>
              {player.name === props.gameSetup.strikeBatsman ||
              player.name === props.gameSetup.nonStrikeBatsman ? (
                <>
                  <Image
                    src={StrikeBatsmanIcon}
                    avatar
                    data-testid="strike-icon"
                  />
                  <span data-testid="strike-batsman">{player.name}</span>
                </>
              ) : (
                <StyledBatsman>{player.name}</StyledBatsman>
              )}
            </Table.Cell>
            <Table.Cell>
              {player.battingStats.balls > 0 ? player.battingStats.runs : ''}
            </Table.Cell>
            <Table.Cell>
              {player.battingStats.balls > 0 ? player.battingStats.balls : ''}
            </Table.Cell>
            <Table.Cell>
              {player.battingStats.balls > 0 ? player.battingStats.fours : ''}
            </Table.Cell>
            <Table.Cell>
              {player.battingStats.balls > 0 ? player.battingStats.sixes : ''}
            </Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </StyledTable>
  )
}

const ExtrasCard: React.FC<{ teamInnings: TeamInnings }> = ({
  teamInnings,
}) => {
  const extras = sumUpExtras(teamInnings)
  return (
    <StyledTable singleLine definition unstackable color="red">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Extras</Table.HeaderCell>
          <Table.HeaderCell>N</Table.HeaderCell>
          <Table.HeaderCell>W</Table.HeaderCell>
          <Table.HeaderCell>L</Table.HeaderCell>
          <Table.HeaderCell>B</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell>Total {extras.total}</Table.Cell>
          <Table.Cell>{extras.noballs}</Table.Cell>
          <Table.Cell>{extras.wides}</Table.Cell>
          <Table.Cell>{extras.legByes}</Table.Cell>
          <Table.Cell>{extras.byes}</Table.Cell>
        </Table.Row>
      </Table.Body>
    </StyledTable>
  )
}

export const BattingTeam = () => {
  const { teams, gameSetup, score } = useSelector<RootState, RootState>(
    (state) => state
  )

  const noOfBallsInOverBattingTeam = noOfLegitimateBallsInOver(
    score.battingTeam.currentOver
  )
  return (
    <StyledTeamContainer data-testid="battingTeam">
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column textAlign="left">
            <Header as="h3">
              {teams.teams[gameSetup.teamIndexBatting].name}
            </Header>
          </Grid.Column>
          <Grid.Column textAlign="right">
            <Header as="h3" data-testid="batting-team-score">
              {score.battingTeam.score}/{score.battingTeam.wickets} in{' '}
              {score.battingTeam.over}
              {noOfBallsInOverBattingTeam > 0 &&
                `.${noOfBallsInOverBattingTeam}`}
            </Header>
          </Grid.Column>
        </Grid.Row>
      </Grid>

      <BattingCard
        gameSetup={gameSetup}
        players={teams.teams[gameSetup.teamIndexBatting].players}
      />
      <BowlingCard
        gameSetup={gameSetup}
        players={teams.teams[gameSetup.teamIndexBowling].players}
      />
      <ExtrasCard teamInnings={score.battingTeam} />
    </StyledTeamContainer>
  )
}

export const BowlingTeam = () => {
  const { teams, gameSetup, score } = useSelector<RootState, RootState>(
    (state) => state
  )
  const noOfBallsInOverBowlingTeam = noOfLegitimateBallsInOver(
    score.bowlingTeam.currentOver
  )
  return (
    <StyledTeamContainer data-testid="bowlingTeam">
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column textAlign="left">
            <Header as="h3">
              {teams.teams[gameSetup.teamIndexBowling].name}
            </Header>
          </Grid.Column>
          <Grid.Column textAlign="right">
            <Header as="h4" data-testid="bowling-team-score">
              {score.bowlingTeam.over > 0 ||
              score.bowlingTeam.currentOver.length > 0 ? (
                <>
                  {score.bowlingTeam.score}/{score.bowlingTeam.wickets} in{' '}
                  {score.bowlingTeam.over}
                  {noOfBallsInOverBowlingTeam > 0 &&
                    `.${noOfBallsInOverBowlingTeam}`}
                </>
              ) : (
                'not played yet'
              )}
            </Header>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <BattingCard
        gameSetup={gameSetup}
        players={teams.teams[gameSetup.teamIndexBowling].players}
      />
      <BowlingCard
        gameSetup={gameSetup}
        players={teams.teams[gameSetup.teamIndexBatting].players}
      />
      <ExtrasCard teamInnings={score.bowlingTeam} />
    </StyledTeamContainer>
  )
}

const createPanes = (gameSetup: GameSetupState, teams: TeamsState) => [
  {
    menuItem: `${teams.teams[gameSetup.teamIndexBatting].name} Innings`,
    render: () => (
      <Tab.Pane attached={false}>
        <BattingTeam />
      </Tab.Pane>
    ),
  },
  {
    menuItem: `${teams.teams[gameSetup.teamIndexBowling].name} Innings`,
    render: () => (
      <Tab.Pane attached={false}>
        <BowlingTeam />
      </Tab.Pane>
    ),
  },
]

const Scorecard = () => {
  const { teams, gameSetup } = useSelector<RootState, RootState>(
    (state) => state
  )
  return (
    <>
      <Tab menu={{ secondary: true }} panes={createPanes(gameSetup, teams)} />
    </>
  )
}

export default Scorecard
