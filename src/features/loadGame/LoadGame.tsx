import React, { useEffect, useState } from 'react'
import { db } from '../../app/database'
import styled from 'styled-components'
import { Button, Header } from 'semantic-ui-react'
import { loadGameAsyncThunk } from './loadGameSlice'
import { useHistory } from 'react-router-dom'
import { useDispatch } from 'react-redux'

const StyledDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
`

const StyledButton = styled(Button)`
  &&& {
    margin-bottom: 30px;
  }
`

const LoadGame = () => {
  const [games, setGames] = useState<string[]>([])
  useEffect(() => {
    async function getGameNames() {
      const gamesFromDb = await db.getGameNames()
      setGames(gamesFromDb)
    }
    getGameNames()
  }, [])
  const dispatch = useDispatch()
  const history = useHistory()
  return (
    <>
      <Header as="h2" content="Games scored on this device" />
      <StyledDiv>
        {games.map((game) => (
          <StyledButton
            key={game}
            primary
            content={game}
            onClick={() => {
              dispatch(loadGameAsyncThunk({ gameName: game, history }))
            }}
          />
        ))}
      </StyledDiv>
    </>
  )
}

export default LoadGame
