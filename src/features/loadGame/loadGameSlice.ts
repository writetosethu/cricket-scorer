import { createAction, createAsyncThunk } from '@reduxjs/toolkit'
import { db } from '../../app/database'
import { History } from 'history'
import { RootState } from '../../app/store'

export interface LoadGameActionPayload {
  gameName: string
  history: History
}

export const loadedStateAction = createAction<
  RootState,
  'loadgame/loadedStateAction'
>('loadgame/loadedStateAction')

export const loadGameAsyncThunk = createAsyncThunk(
  'loadGame',
  async (payload: LoadGameActionPayload, { dispatch }) => {
    const gameState = await db.getGameState(payload.gameName)
    dispatch(loadedStateAction(gameState))
    payload.history.push('/score/edit')
  }
)
