import React from 'react'
import { renderWithState } from '../../testHelpers/renderHelpers'
import Team from './Team'
import { within } from '@testing-library/dom'
import userEvent from '@testing-library/user-event'
import _range from 'lodash/range'

describe('Team Tests', function () {
  it('saves the team names and player names to the state', () => {
    const { store, getByTestId, debug } = renderWithState(<Team />)
    const team1 = within(getByTestId('team1'))

    userEvent.type(team1.getByLabelText('Name'), 'Supersonics')
    _range(11).forEach((index) => {
      userEvent.type(
        team1.getByLabelText(`Player ${index + 1}`),
        `Player ${index + 1}`
      )
    })

    const team2 = within(getByTestId('team2'))

    userEvent.type(team2.getByLabelText('Name'), 'Rouse Hill')
    _range(11).forEach((index) => {
      userEvent.type(
        team2.getByLabelText(`Player ${index + 1}`),
        `Player ${index + 1}`
      )
    })

    let teams = store.getState().teams
    expect(teams.teams[0].name).toEqual('Supersonics')
    expect(teams.teams[1].name).toEqual('Rouse Hill')
    _range(11).forEach((index) => {
      expect(teams.teams[0].players[index].name).toEqual(`Player ${index + 1}`)
    })
    _range(11).forEach((index) => {
      expect(teams.teams[1].players[index].name).toEqual(`Player ${index + 1}`)
    })
  })
})
