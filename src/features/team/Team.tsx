import React from 'react'
import { Button, Card, Form } from 'semantic-ui-react'
import _range from 'lodash/range'
import { teamSlice, TeamsState } from './teamSlice'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../app/store'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'

// TODO: 1. make sure player names are unique
//       2. Allow for more / less than 11 players per team

const StyledButton = styled(Button)`
  &&& {
    margin-top: 10px;
    margin-bottom: 20px;
  }
`

const Team = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const teamsState = useSelector<RootState, TeamsState>((state) => state.teams)
  return (
    <>
      <Card.Group itemsPerRow={1}>
        {_range(2).map((teamIndex) => (
          <Card fluid key={`Team ${teamIndex + 1}`}>
            <Card.Content>
              <Card.Header content={`Team ${teamIndex + 1}`} />
              <Card.Description>
                <Form data-testid={`team${teamIndex + 1}`}>
                  <Form.Field>
                    <label htmlFor={`team${teamIndex + 1}Name`}>Name</label>
                    <input
                      id={`team${teamIndex + 1}Name`}
                      name={`team${teamIndex + 1}Name`}
                      value={teamsState.teams[teamIndex].name}
                      onChange={(event) => {
                        dispatch(
                          teamSlice.actions.setTeamName({
                            teamIndex,
                            name: event.target.value,
                          })
                        )
                      }}
                    />
                  </Form.Field>
                  {_range(11).map((playerIndex) => (
                    <Form.Field
                      key={`team${teamIndex + 1}Player${playerIndex}`}
                    >
                      <label
                        htmlFor={`team${teamIndex + 1}Player${playerIndex}`}
                      >
                        Player {playerIndex + 1}
                      </label>
                      <input
                        id={`team${teamIndex + 1}Player${playerIndex}`}
                        name={`team${teamIndex + 1}Player${playerIndex}`}
                        value={
                          teamsState.teams[teamIndex].players[playerIndex].name
                        }
                        onChange={(event) =>
                          dispatch(
                            teamSlice.actions.setPlayerName({
                              teamIndex,
                              playerIndex,
                              name: event.target.value,
                            })
                          )
                        }
                      />
                    </Form.Field>
                  ))}
                </Form>
              </Card.Description>
            </Card.Content>
          </Card>
        ))}
      </Card.Group>
      <StyledButton
        primary
        content="Next"
        onClick={() => history.push('/gameSetup')}
      />
    </>
  )
}

export default Team
