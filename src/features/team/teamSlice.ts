import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import _range from 'lodash/range'
import {
  DismissalType,
  dismissedByBowler,
  ExtrasType,
  NextBallActionPayload,
  NextOverChangePayload,
  scoreSlice,
} from '../score/scoreSlice'
import { loadedStateAction } from '../loadGame/loadGameSlice'
import { RootState } from '../../app/store'

export interface BattingStats {
  runs: number
  balls: number
  fours: number
  sixes: number
  howOut: DismissalType | null
  wicketBy: PlayerName | null
  caughtBy: PlayerName | null
}

export interface BowlingStats {
  runs: number
  overs: number
  maidens: number
  wickets: number
  ballInCurrentOver: number
}

export type PlayerName = string
export interface PlayerState {
  name: PlayerName
  battingStats: BattingStats
  bowlingStats: BowlingStats
}
export interface TeamState {
  name: string
  players: PlayerState[]
}
export interface TeamsState {
  teams: TeamState[]
}

export const battingStatsInitialState: BattingStats = {
  balls: 0,
  runs: 0,
  fours: 0,
  sixes: 0,
  howOut: null,
  caughtBy: null,
  wicketBy: null,
}

export const bowlingStatsInitialState: BowlingStats = {
  maidens: 0,
  overs: 0,
  runs: 0,
  wickets: 0,
  ballInCurrentOver: 0,
}

const initialState: TeamsState = {
  teams: [
    {
      name: 'Team 1',
      players: _range(11).map((index) => ({
        name: `Team1 Player${index + 1}`,
        battingStats: { ...battingStatsInitialState },
        bowlingStats: { ...bowlingStatsInitialState },
      })),
    },
    {
      name: 'Team 2',
      players: _range(11).map((index) => ({
        name: `Team2 Player${index + 1}`,
        battingStats: { ...battingStatsInitialState },
        bowlingStats: { ...bowlingStatsInitialState },
      })),
    },
  ],
}

export interface TeamNameActionPayload {
  teamIndex: number
  name: string
}
export interface PlayerNameActionPayload {
  teamIndex: number
  playerIndex: number
  name: string
}

export const teamSlice = createSlice({
  name: 'team',
  initialState,
  reducers: {
    setTeamName(state, action: PayloadAction<TeamNameActionPayload>) {
      state.teams[action.payload.teamIndex].name = action.payload.name
    },
    setPlayerName(state, action: PayloadAction<PlayerNameActionPayload>) {
      state.teams[action.payload.teamIndex].players[
        action.payload.playerIndex
      ].name = action.payload.name
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(
        scoreSlice.actions.nextBallAction,
        (state, { payload }: PayloadAction<NextBallActionPayload>) => {
          const strikeBatsman = state.teams[
            payload.teamIndexBatting
          ].players.find((player) => player.name === payload.strikeBatsman)
          const nonStrikeBatsman = state.teams[
            payload.teamIndexBatting
          ].players.find((player) => player.name === payload.nonStrikeBatsman)
          if (
            payload.changeBatterDetails !== null &&
            strikeBatsman !== null &&
            nonStrikeBatsman !== null
          ) {
            let playerOut = strikeBatsman!
            if (
              payload.changeBatterDetails.batterOutName ===
              nonStrikeBatsman!.name
            ) {
              playerOut = nonStrikeBatsman!
            }
            playerOut.battingStats.howOut = payload.changeBatterDetails.howOut
            if (dismissedByBowler(payload.changeBatterDetails.howOut)) {
              playerOut.battingStats.wicketBy = payload.bowler
            }
          }

          if (strikeBatsman != null) {
            if (payload.extra !== null) {
              switch (payload.extra) {
                case ExtrasType.Wide:
                case ExtrasType.NoBall:
                  break

                case ExtrasType.LegBye:
                case ExtrasType.Bye:
                  strikeBatsman.battingStats.balls += 1
                  break

                case ExtrasType.NoBallStruckByBatsman:
                  let runsAgainstBatsman = payload.runs - 1
                  if (runsAgainstBatsman === 4) {
                    strikeBatsman.battingStats.fours += 1
                  }
                  if (runsAgainstBatsman === 6) {
                    strikeBatsman.battingStats.sixes += 1
                  }
                  strikeBatsman.battingStats.runs += runsAgainstBatsman
                  strikeBatsman.battingStats.balls += 1
              }
            } else {
              strikeBatsman.battingStats.runs += payload.runs
              strikeBatsman.battingStats.balls += 1
              if (payload.runs === 4) {
                strikeBatsman.battingStats.fours += 1
              }
              if (payload.runs === 6) {
                strikeBatsman.battingStats.sixes += 1
              }
            }
          }

          const bowler = state.teams[1 - payload.teamIndexBatting].players.find(
            (player) => player.name === payload.bowler
          )
          if (bowler != null) {
            if (
              payload.changeBatterDetails !== null &&
              dismissedByBowler(payload.changeBatterDetails.howOut)
            ) {
              bowler.bowlingStats.wickets += 1
            }

            switch (payload.extra) {
              case ExtrasType.Wide:
              case ExtrasType.NoBall:
              case ExtrasType.NoBallStruckByBatsman:
                bowler.bowlingStats.runs += payload.runs
                break

              case ExtrasType.LegBye:
              case ExtrasType.Bye:
                bowler.bowlingStats.ballInCurrentOver += 1
                break

              default:
                bowler.bowlingStats.ballInCurrentOver += 1
                bowler.bowlingStats.runs += payload.runs
            }
          }
        }
      )
      .addCase(
        scoreSlice.actions.nextOverChange,
        (state, { payload }: PayloadAction<NextOverChangePayload>) => {
          const bowler = state.teams[1 - payload.teamIndexBatting].players.find(
            (player) => player.name === payload.currentBowler
          )
          if (bowler != null) {
            bowler.bowlingStats.overs += 1
            bowler.bowlingStats.ballInCurrentOver = 0
          }
        }
      )
      .addCase(loadedStateAction, (state, action: PayloadAction<RootState>) => {
        state.teams = action.payload.teams.teams
      })
  },
})
