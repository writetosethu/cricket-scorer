import React, { useState } from 'react'
import { Header, Item, Pagination, Tab } from 'semantic-ui-react'
import { useSelector } from 'react-redux'
import { RootState } from '../../app/store'
import {
  getBallsInOver,
  getNumberOfOversBowled,
  ScoreState,
  TeamInnings,
} from '../score/scoreSlice'
import { PaginationProps } from 'semantic-ui-react/dist/commonjs/addons/Pagination/Pagination'
import { GameSetupState } from '../gameSetup/gameSetupSlice'
import { TeamsState } from '../team/teamSlice'

interface BallSummaryProps {
  teamName: string
  teamInnings: TeamInnings
}

const BallSummary: React.FC<BallSummaryProps> = (props) => {
  const numberOfOversBowled = getNumberOfOversBowled(props.teamInnings)
  const [overShown, setOverShown] = useState<number>(numberOfOversBowled)
  const balls = getBallsInOver(overShown, props.teamInnings)
  return (
    <>
      <Header as="h3">{props.teamName}</Header>
      {numberOfOversBowled > 0 && (
        <>
          <Header as="h4">Over {overShown}</Header>
          <Header as="h4">Bowled by {balls[0].bowler}</Header>
          <Item.Group>
            {balls.map((ball, index) => (
              <Item key={index}>
                <Item.Content>
                  <Item.Header as="a">Ball {ball.ball}</Item.Header>
                  <Item.Description>
                    <p>Striker {ball.strikeBatsman}</p>
                    <p>Non Striker {ball.nonStrikeBatsman}</p>
                    <p>
                      Runs <strong>{ball.runs}</strong>
                    </p>
                    {ball.extraType !== null && <p>Extra {ball.extraType}</p>}
                    {ball.dismissalType !== null && (
                      <>
                        <p>
                          {ball.batsmanOut} out by{' '}
                          <strong>{ball.dismissalType}</strong>
                        </p>
                        <p>Incoming batsman {ball.incomingBatsman}</p>
                      </>
                    )}
                  </Item.Description>
                </Item.Content>
              </Item>
            ))}
          </Item.Group>
          <Pagination
            totalPages={numberOfOversBowled}
            activePage={overShown}
            onPageChange={(
              event: React.MouseEvent<HTMLAnchorElement>,
              data: PaginationProps
            ) => {
              setOverShown(data.activePage as number)
            }}
          />
        </>
      )}
    </>
  )
}

const createPanes = (
  gameSetup: GameSetupState,
  teams: TeamsState,
  score: ScoreState
) => [
  {
    menuItem: `${teams.teams[gameSetup.teamIndexBatting].name} Innings`,
    render: () => (
      <Tab.Pane attached={false}>
        <BallSummary
          teamInnings={score.battingTeam}
          teamName={teams.teams[gameSetup.teamIndexBatting].name}
        />
      </Tab.Pane>
    ),
  },
  {
    menuItem: `${teams.teams[gameSetup.teamIndexBowling].name} Innings`,
    render: () => (
      <Tab.Pane attached={false}>
        <BallSummary
          teamInnings={score.bowlingTeam}
          teamName={teams.teams[gameSetup.teamIndexBowling].name}
        />
      </Tab.Pane>
    ),
  },
]

const BallSummaryContainer = () => {
  const { teams, gameSetup, score } = useSelector<RootState, RootState>(
    (state) => state
  )
  return (
    <>
      <Tab
        menu={{ secondary: true }}
        panes={createPanes(gameSetup, teams, score)}
      />
    </>
  )
}

export default BallSummaryContainer
