import React from 'react'
import { renderWithState } from '../../testHelpers/renderHelpers'
import GameSetup from './GameSetup'
import { teamsState } from '../../testHelpers/testData'
import { getAllOptionValues } from '../../testHelpers/formHelpers'
import userEvent from '@testing-library/user-event'
import { waitFor } from '@testing-library/dom'

describe('Game Setup Tests', function () {
  it('displays the team names to select who is batting', () => {
    const { getByText, getByPlaceholderText } = renderWithState(<GameSetup />, {
      teams: teamsState,
    })
    expect(getByText('Team1')).toBeInTheDocument()
    expect(getByText('Team2')).toBeInTheDocument()
    const strikeBatsmenList = getAllOptionValues(
      () => getByPlaceholderText('Strike') as HTMLSelectElement
    )
    const nonStrikeBatsmenList = getAllOptionValues(
      () => getByPlaceholderText('Non strike') as HTMLSelectElement
    )
    const bowlerList = getAllOptionValues(
      () => getByPlaceholderText('Select bowler') as HTMLSelectElement
    )
    expect(strikeBatsmenList).toHaveLength(11)
    expect(strikeBatsmenList).toEqual(nonStrikeBatsmenList)
    expect(strikeBatsmenList[0]).toEqual('1.1')
    expect(strikeBatsmenList[10]).toEqual('1.11')

    expect(bowlerList).toHaveLength(11)
    expect(bowlerList[0]).toEqual('2.1')
    expect(bowlerList[10]).toEqual('2.11')
  })

  it('changes the team batting when the other team is clicked', async () => {
    const { getByText, getByPlaceholderText } = renderWithState(<GameSetup />, {
      teams: teamsState,
    })

    userEvent.click(getByText('Team2'))

    await waitFor(() => {
      const strikeBatsmenList = getAllOptionValues(
        () => getByPlaceholderText('Strike') as HTMLSelectElement
      )
      const nonStrikeBatsmenList = getAllOptionValues(
        () => getByPlaceholderText('Non strike') as HTMLSelectElement
      )
      const bowlerList = getAllOptionValues(
        () => getByPlaceholderText('Select bowler') as HTMLSelectElement
      )
      expect(strikeBatsmenList).toHaveLength(11)
      expect(strikeBatsmenList).toEqual(nonStrikeBatsmenList)
      expect(strikeBatsmenList[0]).toEqual('2.1')
      expect(strikeBatsmenList[10]).toEqual('2.11')

      expect(bowlerList).toHaveLength(11)
      expect(bowlerList[0]).toEqual('1.1')
      expect(bowlerList[10]).toEqual('1.11')
    })
  })

  it('set the state with game setup info', () => {
    const { getByText, getByPlaceholderText, store } = renderWithState(
      <GameSetup />,
      {
        teams: teamsState,
      }
    )

    userEvent.click(getByText('Team2'))
    userEvent.selectOptions(getByPlaceholderText('Strike'), '2.2')
    userEvent.selectOptions(getByPlaceholderText('Non strike'), '2.3')
    userEvent.selectOptions(getByPlaceholderText('Select bowler'), '1.10')

    const state = store.getState().gameSetup
    expect(state.teamIndexBatting).toEqual(1)
    expect(state.teamIndexBowling).toEqual(0)
    expect(state.strikeBatsman).toEqual('2.2')
    expect(state.nonStrikeBatsman).toEqual('2.3')
    expect(state.currentBowler).toEqual('1.10')
  })
})
