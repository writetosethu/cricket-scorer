import React from 'react'
import { Header, Button, Dropdown } from 'semantic-ui-react'
import styled from 'styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../app/store'
import { TeamsState } from '../team/teamSlice'
import { GameSetupState, gameSetupSlice } from './gameSetupSlice'
import { dropdownOption } from '../../app/helpers'
import { DropdownProps } from 'semantic-ui-react/dist/commonjs/modules/Dropdown/Dropdown'
import { useHistory } from 'react-router-dom'

const StyledStartScoringButton = styled(Button)`
  &&& {
    margin-top: 10px;
    margin-bottom: 20px;
  }
`

const StyledButton = styled(Button)`
  &&& {
    width: 40%;
  }
`
const StyledSelect = styled(Dropdown)`
  &&& {
    margin-bottom: 20px;
  }
`
const GameSetup = () => {
  const teamsState = useSelector<RootState, TeamsState>((state) => state.teams)
  const gameSetup = useSelector<RootState, GameSetupState>(
    (state) => state.gameSetup
  )
  const battingPlayers = teamsState.teams[
    gameSetup.teamIndexBatting
  ].players.map((player) => dropdownOption(player.name))

  const bowlingPlayers = teamsState.teams[
    gameSetup.teamIndexBowling
  ].players.map((player) => dropdownOption(player.name))

  const history = useHistory()
  const dispatch = useDispatch()
  return (
    <>
      <Header as="h2">Who is batting?</Header>
      <Button.Group fluid>
        <StyledButton
          toggle
          active={gameSetup.teamIndexBatting === 0}
          onClick={() =>
            dispatch(gameSetupSlice.actions.setBattingTeam({ teamIndex: 0 }))
          }
        >
          {teamsState.teams[0].name}
        </StyledButton>
        <Button.Or text="or" />
        <StyledButton
          toggle
          active={gameSetup.teamIndexBatting === 1}
          onClick={() =>
            dispatch(gameSetupSlice.actions.setBattingTeam({ teamIndex: 1 }))
          }
        >
          {teamsState.teams[1].name}
        </StyledButton>
      </Button.Group>
      <Header as="h2">First batsmen in</Header>
      <StyledSelect
        fluid
        placeholder="Strike"
        options={battingPlayers}
        onChange={(
          event: React.SyntheticEvent<HTMLElement>,
          data: DropdownProps
        ) =>
          dispatch(
            gameSetupSlice.actions.setStrikeBatsman({
              name: data.value as string,
            })
          )
        }
      />
      <StyledSelect
        fluid
        placeholder="Non strike"
        options={battingPlayers}
        onChange={(
          event: React.SyntheticEvent<HTMLElement>,
          data: DropdownProps
        ) =>
          dispatch(
            gameSetupSlice.actions.setNonStrikeBatsman({
              name: data.value as string,
            })
          )
        }
      />
      <Header as="h2">First bowler</Header>
      <StyledSelect
        fluid
        placeholder="Select bowler"
        options={bowlingPlayers}
        onChange={(
          event: React.SyntheticEvent<HTMLElement>,
          data: DropdownProps
        ) =>
          dispatch(
            gameSetupSlice.actions.setBowler({ name: data.value as string })
          )
        }
      />
      <StyledStartScoringButton
        primary
        content="Next"
        onClick={() => history.push('/score/edit')}
      />
    </>
  )
}

export default GameSetup
