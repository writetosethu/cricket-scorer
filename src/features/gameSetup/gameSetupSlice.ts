import { PlayerName } from '../team/teamSlice'
import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import {
  isWideOrNoBall,
  NextBallActionPayload,
  NextOverChangePayload,
  scoreSlice,
} from '../score/scoreSlice'
import { loadedStateAction } from '../loadGame/loadGameSlice'
import { RootState } from '../../app/store'

export interface GameSetupState {
  teamIndexBatting: number
  teamIndexBowling: number
  strikeBatsman: PlayerName | null
  nonStrikeBatsman?: PlayerName | null
  currentBowler?: PlayerName | null
}

const initialState: GameSetupState = {
  teamIndexBatting: 0,
  teamIndexBowling: 1,
  strikeBatsman: null,
  nonStrikeBatsman: null,
  currentBowler: null,
}

export interface SetBattingTeamPayload {
  teamIndex: number
}

export interface PlayerNamePayload {
  name: PlayerName
}

export const gameSetupSlice = createSlice({
  name: 'gameSetup',
  initialState,
  reducers: {
    setBattingTeam(state, action: PayloadAction<SetBattingTeamPayload>) {
      state.teamIndexBatting = action.payload.teamIndex
      state.teamIndexBowling = 1 - action.payload.teamIndex
    },
    setStrikeBatsman(state, action: PayloadAction<PlayerNamePayload>) {
      state.strikeBatsman = action.payload.name
    },
    setNonStrikeBatsman(state, action: PayloadAction<PlayerNamePayload>) {
      state.nonStrikeBatsman = action.payload.name
    },
    setBowler(state, action: PayloadAction<PlayerNamePayload>) {
      state.currentBowler = action.payload.name
    },
    batsmenCrossedOver(state, _) {
      const strikeBatsman = state.strikeBatsman
      state.strikeBatsman = state.nonStrikeBatsman as string
      state.nonStrikeBatsman = strikeBatsman
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(
        scoreSlice.actions.nextOverChange,
        (state, action: PayloadAction<NextOverChangePayload>) => {
          state.currentBowler = action.payload.nextBowler
          const strikeBatsman = state.strikeBatsman
          state.strikeBatsman = state.nonStrikeBatsman as string
          state.nonStrikeBatsman = strikeBatsman
        }
      )
      .addCase(
        scoreSlice.actions.nextBallAction,
        (state, { payload }: PayloadAction<NextBallActionPayload>) => {
          if (payload.changeBatterDetails !== null) {
            if (
              state.strikeBatsman === payload.changeBatterDetails.batterOutName
            ) {
              state.strikeBatsman = payload.changeBatterDetails.nextBatterInName
            } else {
              state.nonStrikeBatsman =
                payload.changeBatterDetails.nextBatterInName
            }
          } else {
            const runsToCheckChangeOfStrike = isWideOrNoBall(payload.extra)
              ? payload.runs - 1
              : payload.runs
            if (runsToCheckChangeOfStrike % 2 === 1) {
              const strikeBatsman = state.strikeBatsman
              state.strikeBatsman = state.nonStrikeBatsman as string
              state.nonStrikeBatsman = strikeBatsman
            }
          }
        }
      )
      .addCase(scoreSlice.actions.changeInnings, (state) => {
        state.teamIndexBowling = 1 - state.teamIndexBowling
        state.teamIndexBatting = 1 - state.teamIndexBowling
        state.strikeBatsman = null
        state.nonStrikeBatsman = null
        state.currentBowler = null
      })
      .addCase(loadedStateAction, (state, action: PayloadAction<RootState>) => {
        state.teamIndexBatting = action.payload.gameSetup.teamIndexBatting
        state.teamIndexBowling = action.payload.gameSetup.teamIndexBowling
        state.strikeBatsman = action.payload.gameSetup.strikeBatsman
        state.nonStrikeBatsman = action.payload.gameSetup.nonStrikeBatsman
        state.currentBowler = action.payload.gameSetup.currentBowler
      })
  },
})
