import { screen, within } from '@testing-library/dom'
import userEvent from '@testing-library/user-event'

export function expectBatsmen(
  runs: number,
  balls: number,
  fours: number,
  sixes: number,
  batsmanName: string,
  getByText = screen.getByText
) {
  const batsmanRow = getByText(batsmanName).closest('tr') as HTMLTableRowElement
  expect(parseInt(batsmanRow.cells[1].textContent as string)).toEqual(runs)
  expect(parseInt(batsmanRow.cells[2].textContent as string)).toEqual(balls)
  expect(parseInt(batsmanRow.cells[3].textContent as string)).toEqual(fours)
  expect(parseInt(batsmanRow.cells[4].textContent as string)).toEqual(sixes)
}

export function expectBowler(
  runs: number,
  overs: string,
  maidens: number,
  wickets: number,
  bowlerName: string,
  getByText = screen.getByText
) {
  const bowlerRow = getByText(bowlerName).closest('tr') as HTMLTableRowElement
  expect(bowlerRow.cells[1].textContent as string).toEqual(overs)
  expect(parseInt(bowlerRow.cells[2].textContent as string)).toEqual(runs)
  expect(parseInt(bowlerRow.cells[3].textContent as string)).toEqual(maidens)
  expect(parseInt(bowlerRow.cells[4].textContent as string)).toEqual(wickets)
}

export function scoreAnOver(runs: number[]) {
  const buttonContainer = within(screen.getByTestId('button-container'))

  for (let i = 0; i < runs.length; i++) {
    userEvent.click(buttonContainer.getByText(runs[i]))
    userEvent.click(buttonContainer.getByText('Next ball'))
  }
}

export function expectBattingTeamScore(score: string) {
  expect(screen.getByTestId('batting-team-score').textContent).toEqual(score)
}
