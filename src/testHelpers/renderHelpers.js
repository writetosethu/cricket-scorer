import React from 'react'
import { MemoryRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { render } from '@testing-library/react'
import { createStore } from '../app/store'

export function renderWithRouter(Component, route = '/') {
  return render(
    <MemoryRouter initialEntries={[route]}>{Component}</MemoryRouter>
  )
}

export const renderWithState = (Component, initialState = {}, route = '/') => {
  let store = createStore()
  store.dispatch({ type: 'REDUX_INIT' })
  store = createStore({ ...store.getState(), ...initialState })
  return {
    ...render(
      <Provider store={store}>
        <MemoryRouter initialEntries={[route]}>{Component}</MemoryRouter>
      </Provider>
    ),
    store,
  }
}
