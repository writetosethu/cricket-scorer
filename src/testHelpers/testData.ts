import {
  battingStatsInitialState,
  bowlingStatsInitialState,
  TeamsState,
} from '../features/team/teamSlice'
import { GameSetupState } from '../features/gameSetup/gameSetupSlice'
import _range from 'lodash/range'

export const teamsState: TeamsState = {
  teams: [
    {
      name: 'Team1',
      players: _range(11).map((index) => ({
        name: `1.${index + 1}`,
        battingStats: { ...battingStatsInitialState },
        bowlingStats: { ...bowlingStatsInitialState },
      })),
    },
    {
      name: 'Team2',
      players: _range(11).map((index) => ({
        name: `2.${index + 1}`,
        battingStats: { ...battingStatsInitialState },
        bowlingStats: { ...bowlingStatsInitialState },
      })),
    },
  ],
}

export const gameSetupState: GameSetupState = {
  currentBowler: '1.1',
  strikeBatsman: '2.1',
  nonStrikeBatsman: '2.2',
  teamIndexBowling: 0,
  teamIndexBatting: 1,
}
