import userEvent from '@testing-library/user-event'
import { screen } from '@testing-library/dom'

export const setValuesInForm = (formDataKeyedByLabel: {
  [key: string]: [string]
}) => {
  Object.keys(formDataKeyedByLabel).map((label) => {
    const input = screen.getByLabelText(label)
    userEvent.type(input, (formDataKeyedByLabel[label] as unknown) as string)
    return null
  })
}

export const getAllOptionValues = (query: () => HTMLSelectElement) => {
  const options = query().options
  const optionValues = []
  for (let i = 0; i < options.length; i++) {
    optionValues.push(options[i].value)
  }
  return optionValues
}
