import { Middleware } from 'redux'
import { RootState } from './store'
// @ts-ignore
import ric from 'ric-shim'
import { db } from './database'

export const persistMiddleware: Middleware<{}, RootState> = (store) => (
  next
) => (action) => {
  next(action)
  // We often don't have any urgency here
  // we can just tell the browser to do this
  // when it's not busy. Which is exactly
  // what requestIdleCallback lets us do.
  ric(() => {
    const appState = store.getState()
    db.storeState(appState)
  })
}
