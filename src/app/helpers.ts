import { DropdownItemProps } from 'semantic-ui-react/dist/commonjs/modules/Dropdown/DropdownItem'

export const dropdownOption = (value: string): DropdownItemProps => ({
  key: value,
  text: value,
  value,
})
