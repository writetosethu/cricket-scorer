import Dexie from 'dexie'
import { RootState } from './store'
import moment from 'moment'

interface GamesTable {
  id: string
  gameState: string
}

class Database extends Dexie {
  games: Dexie.Table<GamesTable, string>

  constructor(databaseName: string) {
    super(databaseName)
    this.version(1).stores({
      games: 'id,gameName,gameState',
    })
    this.games = this.table('games')
  }

  async storeState(state: RootState): Promise<string> {
    return db.games.put({
      id: `${state.teams.teams[0].name}-vs-${
        state.teams.teams[1].name
      }-${moment().format('DD-MM-YYYY')}`,
      gameState: JSON.stringify(state),
    })
  }

  async getGameNames(): Promise<string[]> {
    const games = await db.games.toArray()
    return games.map((game) => game.id)
  }

  async getGameState(gameName: string): Promise<RootState> {
    const game = await db.games.where('id').equals(gameName).first()
    if (game) {
      return JSON.parse(game.gameState)
    }
  }
}

export const db = new Database('cricket-scorer')
