import {
  configureStore,
  ThunkAction,
  Action,
  EnhancedStore,
} from '@reduxjs/toolkit'
import { teamSlice } from '../features/team/teamSlice'
import { gameSetupSlice } from '../features/gameSetup/gameSetupSlice'
import { scoreSlice } from '../features/score/scoreSlice'
import { persistMiddleware } from './persistStateMiddleware'
import { useDispatch } from 'react-redux'

export const createStore = (preloadedState = undefined): EnhancedStore =>
  configureStore({
    reducer: {
      teams: teamSlice.reducer,
      gameSetup: gameSetupSlice.reducer,
      score: scoreSlice.reducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().prepend(persistMiddleware),
    preloadedState,
  })

export const store = createStore()

export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>()
export type RootState = ReturnType<typeof store.getState>

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>
