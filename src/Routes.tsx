import { Redirect, Route, Switch } from 'react-router-dom'
import App from './App'
import Team from './features/team/Team'
import React from 'react'
import GameSetup from 'features/gameSetup/GameSetup'
import LoadGame from './features/loadGame/LoadGame'
import { useSelector } from 'react-redux'
import { RootState } from './app/store'
import { GameSetupState } from './features/gameSetup/gameSetupSlice'
import ScoreContainer from './scoreContainer/ScoreContainer'

export const Routes = () => {
  const gameSetup = useSelector<RootState, GameSetupState>(
    (state) => state.gameSetup
  )
  return (
    <Switch>
      <Route path="/gameSetup" component={GameSetup} />
      <Route path="/team" component={Team} />
      <Route path="/loadGame" component={LoadGame} />
      <Route path="/score">
        {gameSetup.currentBowler == null ? (
          <Redirect to="/" />
        ) : (
          <ScoreContainer />
        )}
      </Route>
      <Route path="/" component={App} />
    </Switch>
  )
}
