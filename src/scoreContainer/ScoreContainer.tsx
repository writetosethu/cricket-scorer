import React, { useState } from 'react'
import { Route, Switch, useHistory, withRouter } from 'react-router-dom'
import Scorecard from '../features/scorecard/Scorecard'
import BallSummaryContainer from '../features/ballSummary/BallSummary'
import Score from '../features/score/Score'
import { Header, Icon, Menu, Segment, Sidebar } from 'semantic-ui-react'
import styled from 'styled-components'
import { RouteComponentProps } from 'react-router'

const StyledDiv = styled.div`
  margin-bottom: 20px;
`

const ScoreContainer: React.FC<RouteComponentProps> = (props) => {
  const [sidebarVisible, setSidebarVisible] = useState<boolean>(false)
  const history = useHistory()
  const menuButtonClicked = (page: string) => {
    setSidebarVisible(false)
    history.push(page)
  }
  return (
    <>
      <StyledDiv>
        <Header as="h2">
          <Header.Content>
            <Icon
              name="bars"
              data-testid="open-sidebar-button"
              onClick={() => {
                setSidebarVisible(true)
              }}
            />{' '}
            {props.location.pathname.endsWith('edit') && 'Score'}
            {props.location.pathname.endsWith('scorecard') && 'Scorecard'}
            {props.location.pathname.endsWith('ballSummary') && 'Ball summary'}
          </Header.Content>
        </Header>
      </StyledDiv>

      <Sidebar.Pushable as={Segment}>
        <Sidebar
          as={Menu}
          animation="overlay"
          icon="labeled"
          inverted
          vertical
          visible={sidebarVisible}
          width="thin"
        >
          <Menu.Item as="a" onClick={() => menuButtonClicked('/score/edit')}>
            <Icon name="edit" />
            Score
          </Menu.Item>
          <Menu.Item
            as="a"
            onClick={() => menuButtonClicked('/score/scorecard')}
          >
            <Icon name="table" />
            Scorecard
          </Menu.Item>
          <Menu.Item
            as="a"
            onClick={() => menuButtonClicked('/score/ballSummary')}
          >
            <Icon name="baseball ball" />
            Ball by Ball Summary
          </Menu.Item>
        </Sidebar>

        <Sidebar.Pusher>
          <Segment basic>
            <Switch>
              <Route path="/score/scorecard" component={Scorecard} />
              <Route path="/score/edit" component={Score} />
              <Route
                path="/score/ballSummary"
                component={BallSummaryContainer}
              />
            </Switch>
          </Segment>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    </>
  )
}

export default withRouter(ScoreContainer)
