import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { store } from './app/store'
import { Provider } from 'react-redux'
import * as serviceWorker from './serviceWorker'
import 'semantic-ui-css/semantic.min.css'
import { Routes } from './Routes'
import 'index.css'
import styled from 'styled-components'

const StyledDiv = styled.div`
  margin: 10px;
`

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <StyledDiv>
        <Routes />
      </StyledDiv>
    </Provider>
  </BrowserRouter>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
