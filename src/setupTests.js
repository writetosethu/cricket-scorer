// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect'
import { db } from './app/database'

jest.mock('semantic-ui-react', () => ({
  ...jest.requireActual('semantic-ui-react'),
  Dropdown: ({ placeholder, name, label, options, value, onChange }) => {
    function handleChange(event) {
      onChange(event, { value: event.target.value })
    }
    return (
      <>
        <label htmlFor={name}>{label}</label>
        <select
          placeholder={placeholder}
          name={name}
          id={name}
          onChange={handleChange}
          value={value}
        >
          {options.map(({ key, value, text }) => (
            <option key={key} value={value}>
              {text}
            </option>
          ))}
        </select>
      </>
    )
  },
}))

jest.mock('./app/database', () => {
  return {
    db: {
      storeState: jest.fn().mockResolvedValue(null),
    },
  }
})

beforeEach(() => {
  jest.clearAllMocks()
})
