import React from 'react'
import { Button, Grid } from 'semantic-ui-react'
import styled from 'styled-components'
import { useHistory } from 'react-router-dom'
import './app/database'

const StyledButton = styled(Button)`
  &&& {
    width: 200px;
    height: 50px;
  }
`

const Centered = styled.div`
  position: absolute;
  width: 300px;
  height: 200px;
  z-index: 15;
  top: 50%;
  left: 50%;
  margin: -100px 0 0 -150px;
`

const App: React.FC = () => {
  const history = useHistory()
  return (
    <Centered>
      <Grid>
        <Grid.Row columns={1} textAlign="center">
          <Grid.Column>
            <StyledButton primary onClick={() => history.push('/team')}>
              Start a new game
            </StyledButton>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={1} textAlign="center">
          <Grid.Column>
            <StyledButton primary onClick={() => history.push('/loadGame')}>
              Load game
            </StyledButton>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Centered>
  )
}

export default App
