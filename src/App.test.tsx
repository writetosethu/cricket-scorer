import React from 'react'
import userEvent from '@testing-library/user-event'
import { renderWithState } from 'testHelpers/renderHelpers'
import { Routes } from './Routes'

describe('App Tests', () => {
  it('when I click start a new game then the team page appears', () => {
    const { getByText } = renderWithState(<Routes />)
    userEvent.click(getByText('Start a new game'))
    expect(getByText('Team 1')).toBeInTheDocument()
  })
})
